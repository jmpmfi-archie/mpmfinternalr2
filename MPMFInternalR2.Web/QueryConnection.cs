﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Globalization;
using System.Web;

namespace MPMFInternalR2.Web
{
    public class QueryConnection : ReportQueryBase
    {
        public System.Data.DataSet QueryArea()
        {
            try
            {

                OpenConnection();
                string strCmd = @"select distinct AREA_CODE as AreaID, AREA_NAME as areafullname 
                             from CONFINS..REF_OFFICE_AREA roa with(nolock) inner join confins..REF_OFFICE ro with(nolock)
                            on roa.REF_OFFICE_AREA_ID = ro.REF_OFFICE_AREA_ID where ro.IS_VIRTUAL_OFFICE = 0 
                             order by AREA_NAME ";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");

                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportPhysicPolicyReceive(string branchid, string startDate, string endDate, string reportType, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.Char, 10).Value = startDate.ToString();
                SQLCommand.Parameters.Add("@endDate", SqlDbType.Char, 10).Value = endDate.ToString();
                SQLCommand.Parameters.Add("@reportType", SqlDbType.Char, 10).Value = reportType;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryBranch(string strOfficeCode)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = "spMPMFBranchList";
                SQLCommand.Parameters.Add("@OFFICE_CODE", SqlDbType.VarChar, 3).Value = strOfficeCode.Trim();
                //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QuerySchemaCode()
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = "uspMPMFSchemeApvList";
                //SQLCommand.Parameters.Add("@CGID", SqlDbType.VarChar, 3).Value = HttpContext.Current.Session["groupdbid"].ToString();
                //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QuerySchemaCode(string SchemaCode)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = "uspMPMFSchemeApvList";
                SQLCommand.Parameters.Add("@SchemeCode", SqlDbType.VarChar, 30).Value = SchemaCode;
                //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryRptDistAsuransi(string branchid, string categoryid, string brand, string period, string status, string paid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@assetCategory", SqlDbType.VarChar, 8000).Value = categoryid;
                SQLCommand.Parameters.Add("@assetBrand", SqlDbType.VarChar, 8000).Value = brand;
                SQLCommand.Parameters.Add("@period", SqlDbType.VarChar, 8000).Value = period;
                SQLCommand.Parameters.Add("@ddlStatus", SqlDbType.VarChar, 8000).Value = status;
                SQLCommand.Parameters.Add("@ddlPaid", SqlDbType.VarChar, 8000).Value = paid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryNotary()
        {
            try
            {
                OpenConnection();
                string strCmd = "select distinct NOTARY_NAME as notaryname from Notary with(nolock) order by NOTARY_NAME";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspAnjCustRptSalesMyCashMotor(string tglfrom, string tglto, string branchid, string order, string Assettypeid, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.VarChar, 10).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.VarChar, 10).Value = tglto;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                SQLCommand.Parameters.Add("@order", SqlDbType.VarChar, 25).Value = order;
                SQLCommand.Parameters.Add("@Assettypeid", SqlDbType.VarChar, 5).Value = Assettypeid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportFiduciaList(string branchid, string startDate, string endDate, string notaryid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 8000).Value = startDate;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 8000).Value = endDate;
                SQLCommand.Parameters.Add("@notaryid", SqlDbType.VarChar, 8000).Value = notaryid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryCategory()
        {
            try
            {
                OpenConnection();
                string strCmd = "select InitialProductCode,DESCRIPTIONPRODUCT from [CONFINSR1].SAFDB.dbo.safassetcategory with(nolock) order by InitialProductCode";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryFID(string sWhereCond, string spName)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spName;
                SQLCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 8000).Value = sWhereCond;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportDetailAgingAR(string branchid, string categoryid, string brand, string period, string status, string paid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@assetCategory", SqlDbType.VarChar, 8000).Value = categoryid;
                SQLCommand.Parameters.Add("@assetBrand", SqlDbType.VarChar, 8000).Value = brand;
                SQLCommand.Parameters.Add("@period", SqlDbType.VarChar, 8000).Value = period;
                SQLCommand.Parameters.Add("@ddlStatus", SqlDbType.VarChar, 8000).Value = status;
                SQLCommand.Parameters.Add("@ddlPaid", SqlDbType.VarChar, 8000).Value = paid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryBrand()
        {
            try
            {
                OpenConnection();
                string strCmd = "Select am.ASSET_CODE as AssetCode, ac.ASSET_CATEGORY_CODE as CategoryID  from CONFINS..ASSET_MASTER am with(nolock) left join	CONFINS..ASSET_CATEGORY ac with(nolock) on am.ASSET_CATEGORY_ID = ac.ASSET_CATEGORY_ID where am.ASSET_HIERARCHY_L1_ID is not null order by ASSET_CODE";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryYear()
        {
            try
            {
                OpenConnection();
                string strCmd = "select month(SYS_VALUE) as month, year(SYS_VALUE) as Year from CONFINS..Sys_ctrl_coy with(nolock)";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportFID(string areaid, string categoryid, string brand, string month, string year, string optionbase, string baseon, string reporttype, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@areaID", SqlDbType.VarChar, 8000).Value = areaid;
                SQLCommand.Parameters.Add("@assetCategory", SqlDbType.VarChar, 8000).Value = categoryid;
                SQLCommand.Parameters.Add("@assetBrand", SqlDbType.VarChar, 8000).Value = brand;
                SQLCommand.Parameters.Add("@ddlMonth", SqlDbType.VarChar, 8000).Value = month;
                SQLCommand.Parameters.Add("@txtYear", SqlDbType.VarChar, 8000).Value = year;
                SQLCommand.Parameters.Add("@baseOnAR", SqlDbType.VarChar, 8000).Value = optionbase;
                SQLCommand.Parameters.Add("@baseOn", SqlDbType.VarChar, 8000).Value = baseon;
                SQLCommand.Parameters.Add("@reporttype", SqlDbType.VarChar, 8000).Value = reporttype;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryReportFIDwBranch(string areaid, string categoryid, string brand, string month, string year, string optionbase, string baseon, string reporttype, string branchid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@areaID", SqlDbType.VarChar, 8000).Value = areaid;
                SQLCommand.Parameters.Add("@assetCategory", SqlDbType.VarChar, 8000).Value = categoryid;
                SQLCommand.Parameters.Add("@assetBrand", SqlDbType.VarChar, 8000).Value = brand;
                SQLCommand.Parameters.Add("@ddlMonth", SqlDbType.VarChar, 8000).Value = month;
                SQLCommand.Parameters.Add("@txtYear", SqlDbType.VarChar, 8000).Value = year;
                SQLCommand.Parameters.Add("@baseOnAR", SqlDbType.VarChar, 8000).Value = optionbase;
                SQLCommand.Parameters.Add("@baseOn", SqlDbType.VarChar, 8000).Value = baseon;
                SQLCommand.Parameters.Add("@reporttype", SqlDbType.VarChar, 8000).Value = reporttype;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportFIDDownpayment(string areaid, string categoryid, string brand, string month, string year, string optionbase, string baseon, string reporttype, string branchid, int pdp1, int pdp2, int pdp3, int pdp4, int pdp5, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@areaID", SqlDbType.VarChar, 8000).Value = areaid;
                SQLCommand.Parameters.Add("@assetCategory", SqlDbType.VarChar, 8000).Value = categoryid;
                SQLCommand.Parameters.Add("@assetBrand", SqlDbType.VarChar, 8000).Value = brand;
                SQLCommand.Parameters.Add("@ddlMonth", SqlDbType.VarChar, 8000).Value = month;
                SQLCommand.Parameters.Add("@txtYear", SqlDbType.VarChar, 8000).Value = year;
                SQLCommand.Parameters.Add("@baseOnAR", SqlDbType.VarChar, 8000).Value = optionbase;
                SQLCommand.Parameters.Add("@baseOn", SqlDbType.VarChar, 8000).Value = baseon;
                SQLCommand.Parameters.Add("@reporttype", SqlDbType.VarChar, 8000).Value = reporttype;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@pdp1", SqlDbType.Int).Value = pdp1;
                SQLCommand.Parameters.Add("@pdp2", SqlDbType.Int).Value = pdp2;
                SQLCommand.Parameters.Add("@pdp3", SqlDbType.Int).Value = pdp3;
                SQLCommand.Parameters.Add("@pdp4", SqlDbType.Int).Value = pdp4;
                SQLCommand.Parameters.Add("@pdp5", SqlDbType.Int).Value = pdp5;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryAreaCollection( string strBranchid)
        {
            try
            {
                if (strBranchid == "'999'")
                {
                    OpenConnection();
                    //string strCmd = "select distinct a.collareaid, collareafullname, collareainitialname from SAFCust_AreaColl a with(nolock) left join branch b with(nolock) on a.collareaid=b.collareaid order by collareafullname";
                    string strCmd = "select distinct a.collareaid, collareafullname, collareainitialname from [ConfinsR1].SAFDB.dbo.SAFCust_AreaColl a with(nolock) order by collareafullname";
                    SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                    SqlAdapt.Fill(oDataset, "Table");
                }
                else
                {
                    OpenConnection();
                    //string strCmd = "select distinct a.collareaid, collareafullname, collareainitialname from SAFCust_AreaColl a with(nolock) left join branch b with(nolock) on a.collareaid=b.collareaid where branchid = " + HttpContext.Current.Session["sesBranchId"].ToString() + " order by collareafullname";
                    string strCmd = "select distinct a.collareaid, collareafullname, collareainitialname from [ConfinsR1].SAFDB.dbo.SAFCust_AreaColl a with(nolock) order by collareafullname";
                    SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                    SqlAdapt.Fill(oDataset, "Table");
                }
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryBranchAreaCollection(string collareaid)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = "spMPMFBranchList";
                SQLCommand.Parameters.Add("@OFFICE_CODE", SqlDbType.VarChar, 3).Value = collareaid;
                //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");

                //if (collareaid.ToString().Trim() == "ALL")
                //{

                //    OpenConnection();
                //    SQLCommand.Connection = SqlConn;
                //    SQLCommand.CommandType = CommandType.StoredProcedure;
                //    SQLCommand.CommandTimeout = 14400;
                //    SQLCommand.CommandText = "spMPMFBranchList";
                //    SQLCommand.Parameters.Add("@OFFICE_CODE", SqlDbType.VarChar, 3).Value = collareaid;
                //    //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //    //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                //    SqlAdapt.SelectCommand = SQLCommand;
                //    SqlAdapt.Fill(oDataset, "Table");

                //}
                //else
                //{
                //    OpenConnection();
                //    SQLCommand.Connection = SqlConn;
                //    SQLCommand.CommandType = CommandType.StoredProcedure;
                //    SQLCommand.CommandTimeout = 14400;
                //    SQLCommand.CommandText = "spMPMFBranchList";
                //    //SQLCommand.Parameters.Add("@CGID", SqlDbType.VarChar, 3).Value = HttpContext.Current.Session["groupdbid"].ToString();
                //    //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //    //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                //    SqlAdapt.SelectCommand = SQLCommand;
                //    SqlAdapt.Fill(oDataset, "Table");

                //}
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryDate()
        {
            try
            {
                OpenConnection();
                string strCmd = "select convert(varchar(10),SYS_VALUE,103) as date from CONFINS..Sys_ctrl_coy with(nolock)";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportCustomerByCollector(string branchid, string periode, string startBucket, string endingBucket, string reporttype, string areaid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@periode", SqlDbType.VarChar, 8000).Value = periode;
                SQLCommand.Parameters.Add("@startbucket", SqlDbType.VarChar, 8000).Value = startBucket;
                SQLCommand.Parameters.Add("@endbucket", SqlDbType.VarChar, 8000).Value = endingBucket;
                SQLCommand.Parameters.Add("@reportType", SqlDbType.VarChar, 8000).Value = reporttype;
                SQLCommand.Parameters.Add("@areaid", SqlDbType.VarChar, 8000).Value = areaid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportRAL(string branchid, string date1, string date2, string criteria, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchId", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 8000).Value = date1;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 8000).Value = date2;
                SQLCommand.Parameters.Add("@criteria", SqlDbType.VarChar, 8000).Value = criteria;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportARByJT(string branchid, string asof, string areaid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@AgingFlow", SqlDbType.VarChar, 8000).Value = asof;
                SQLCommand.Parameters.Add("@areaid", SqlDbType.VarChar, 8000).Value = areaid;
                SqlAdapt.SelectCommand = SQLCommand;
                SQLCommand.CommandTimeout = 14400;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryAssetType()
        {
            try
            {
                OpenConnection();
                string strCmd = "select rtrim(ASSET_TYPE_CODE) as assettypeid, ASSET_TYPE_NAME as description from CONFINS..ASSET_TYPE with(nolock) order by ASSET_TYPE_NAME";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportBJD(string branchid, string startDate, string endDate, string reportType, string cutoff, string spname, string assettypeid)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchId", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 8000).Value = startDate;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 8000).Value = endDate;
                SQLCommand.Parameters.Add("@reportType", SqlDbType.VarChar, 8000).Value = reportType;
                SQLCommand.Parameters.Add("@cutoff", SqlDbType.VarChar, 8000).Value = cutoff;
                SQLCommand.Parameters.Add("@assettypeid", SqlDbType.VarChar, 10).Value = assettypeid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportDenda(string agreementno, string branchid, string type, string year, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@agreementno", SqlDbType.VarChar, 8000).Value = agreementno;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@paid", SqlDbType.VarChar, 8000).Value = type;
                SQLCommand.Parameters.Add("@year", SqlDbType.VarChar, 8000).Value = year;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportFIDEloan(string branchid, string period, string asset, string report, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 20).Value = branchid;
                SQLCommand.Parameters.Add("@agdate", SqlDbType.Char, 20).Value = period;
                SQLCommand.Parameters.Add("@assettype", SqlDbType.Char, 20).Value = asset;
                SQLCommand.Parameters.Add("@reporttype", SqlDbType.Char, 20).Value = report;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryTTRPickUp(string date, string date1, string branchId, string assettypeid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.Char, 20).Value = date;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.Char, 20).Value = date1;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 20).Value = branchId;
                SQLCommand.Parameters.Add("@Assettypeid", SqlDbType.Char, 20).Value = assettypeid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryBranchInnerCollectionGroup(string branchid)
        {
            try
            {
                if (branchid.ToString().Trim() == "'999'")
                //if (HttpContext.Current.Session["sesBranchId"].ToString() == "'999'")
                {
                    OpenConnection();
                    string strCmd = "select * from branch with(nolock) inner join collectiongroup with(nolock) on branch.cgid=collectiongroup.cgid where branch.branchid not in ('999')";
                    SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                    SqlAdapt.Fill(oDataset, "Table");
                }
                else
                {
                    OpenConnection();
                    string strCmd = "select * from branch with(nolock) inner join collectiongroup with(nolock) on branch.cgid=collectiongroup.cgid where branch.branchid not in ('999') and branch.branchid in(" + branchid.ToString().Trim() + ")";
                    //string strCmd = "select * from branch with(nolock) inner join collectiongroup with(nolock) on branch.cgid=collectiongroup.cgid where branch.branchid not in ('999') and branch.branchid = " + HttpContext.Current.Session["sesBranchId"].ToString().Trim() + "";
                    SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                    SqlAdapt.Fill(oDataset, "Table");
                }
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }
        public System.Data.DataTable QueryReportspSAFVisitFeeLCAssetRecovery(string branchid, string period, string assettype, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                SQLCommand.Parameters.Add("@period", SqlDbType.VarChar, 6).Value = period;
                SQLCommand.Parameters.Add("@assetid", SqlDbType.VarChar, 2).Value = assettype;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportInsOffSystem(string branchid, string date1, string date2, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@startdate", SqlDbType.VarChar, 8000).Value = date1;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 8000).Value = date2;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportInsurance(string branchid, string startDate, string endDate, string reportType,
           string orderCond, string policyCond, string groupingCond, string typeCond, string invoiceNo, string rekapDetail, string spname
           )
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchId", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 8000).Value = startDate;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 8000).Value = endDate;
                SQLCommand.Parameters.Add("@reportType", SqlDbType.VarChar, 8000).Value = reportType;
                SQLCommand.Parameters.Add("@orderCond", SqlDbType.VarChar, 8000).Value = orderCond;
                SQLCommand.Parameters.Add("@policyCond", SqlDbType.VarChar, 8000).Value = policyCond;
                SQLCommand.Parameters.Add("@groupingCond", SqlDbType.VarChar, 8000).Value = groupingCond;
                SQLCommand.Parameters.Add("@typeCond", SqlDbType.VarChar, 8000).Value = typeCond;
                SQLCommand.Parameters.Add("@invoiceNo", SqlDbType.VarChar, 8000).Value = invoiceNo;
                SQLCommand.Parameters.Add("@rekapDetail", SqlDbType.VarChar, 8000).Value = rekapDetail;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryReportMutasiJurnal(string branchid, string bulan, string tahun, string report, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 20).Value = branchid;
                SQLCommand.Parameters.Add("@month", SqlDbType.Char, 20).Value = bulan;
                SQLCommand.Parameters.Add("@year", SqlDbType.Char, 20).Value = tahun;
                SQLCommand.Parameters.Add("@report", SqlDbType.Char, 20).Value = report;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportROVisitKonsumen(string branchid, string maturitydate, string maturitydate2, string category, string product, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@maturitydate1", SqlDbType.VarChar, 8000).Value = maturitydate;
                SQLCommand.Parameters.Add("@maturitydate2", SqlDbType.VarChar, 8000).Value = maturitydate2;
                SQLCommand.Parameters.Add("@category", SqlDbType.VarChar, 8000).Value = category;
                SQLCommand.Parameters.Add("@AssetTypeId", SqlDbType.VarChar, 8000).Value = product;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportBookingDana(string branchid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 20).Value = branchid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportPaymentDetail(string branch, string reporttype, string assetCode, string periodtypeid, string date1, string date2, string month, string year, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 20).Value = branch;
                SQLCommand.Parameters.Add("@assetCode", SqlDbType.Char, 20).Value = assetCode;
                SQLCommand.Parameters.Add("@periodtypeid", SqlDbType.Char, 20).Value = periodtypeid;
                SQLCommand.Parameters.Add("@reportType", SqlDbType.Char, 20).Value = reporttype;
                SQLCommand.Parameters.Add("@date1", SqlDbType.Char, 20).Value = date1;
                SQLCommand.Parameters.Add("@date2", SqlDbType.Char, 20).Value = date2;
                SQLCommand.Parameters.Add("@month", SqlDbType.Char, 20).Value = month;
                SQLCommand.Parameters.Add("@year", SqlDbType.Char, 20).Value = year;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable QueryRptInvalidDataIntelix(string branchid, string date1, string date2, string statusid, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@tglfrom", SqlDbType.VarChar, 0x1f40).Value = date1;
                base.SQLCommand.Parameters.Add("@tglto", SqlDbType.VarChar, 0x1f40).Value = date2;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 0x1f40).Value = branchid;
                base.SQLCommand.Parameters.Add("@statusid", SqlDbType.VarChar, 0x1f40).Value = statusid;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public System.Data.DataSet QueryAsset()
        {
            try
            {
                OpenConnection();
                //string strCmd = "Select * FROM assettype with(nolock) where assettypeid in ('1','5')";
                string strCmd = "Select ASSET_TYPE_CODE as AssetTypeId, ASSET_TYPE_NAME as description  FROM ASSET_TYPE with(nolock) ";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFSalesDahsyat(string branchid, string initialproductcode, string startDate, string endDate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 15).Value = branchid;
                SQLCommand.Parameters.Add("@initialproductcode", SqlDbType.VarChar, 10).Value = initialproductcode;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 30).Value = startDate;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 30).Value = endDate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFAssetManagement(string branchid, string startDate, string endDate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 15).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 30).Value = startDate;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 30).Value = endDate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFAssetManagement(string AreaId, string branchid, string startDate, string endDate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 15).Value = branchid;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.VarChar, 15).Value = AreaId;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 30).Value = startDate;
                SQLCommand.Parameters.Add("@endDate", SqlDbType.VarChar, 30).Value = endDate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFAssetManagementStock(string branchid, string startDate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 15).Value = branchid;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 30).Value = startDate;

                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFAssetManagementStock(string AreaId, string branchid, string startDate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 15).Value = branchid;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.VarChar, 15).Value = AreaId;
                SQLCommand.Parameters.Add("@startDate", SqlDbType.VarChar, 30).Value = startDate;

                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable QueryReportspSAFCustCallCenterRptProductivityAgent(string date1, string date2, string datetype, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 0x1f40).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 0x1f40).Value = date2;
                base.SQLCommand.Parameters.Add("@datetype", SqlDbType.VarChar, 0x1f40).Value = datetype;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public DataSet QueryAllArea()
        {
            DataSet oDataset;
            try
            {
                base.OpenConnection();
                string selectCommandText = "selecT distinct ccareaid, ccareaname from [ConfinsR1].SAFDB.dbo.SAFCust_CallCenterArea with(nolock) order by ccareaname";
                base.SqlAdapt = new SqlDataAdapter(selectCommandText, base.SqlConn);
                base.SqlAdapt.Fill(base.oDataset, "Table");
                oDataset = base.oDataset;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return oDataset;
        }


        #region belum di rubah ke confinsr2
        public DataSet QueryAllBranchWhereArea(string areaid)
        {
            DataSet oDataset;
            try
            {
                base.OpenConnection();
                string selectCommandText = "select a.branchid, a.branchfullname from [ConfinsR1].SAFDB.dbo.branch a with(nolock) inner join [ConfinsR1].SAFDB.dbo.SAFCust_CallCenterArea b with(Nolock) on a.branchid = b.ccbranchid where branchid not in('999') and ccareaid ='" + areaid.ToString() + "' order by branchfullname";
                base.SqlAdapt = new SqlDataAdapter(selectCommandText, base.SqlConn);
                base.SqlAdapt.Fill(base.oDataset, "Table");
                oDataset = base.oDataset;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return oDataset;
        }
        #endregion

        public System.Data.DataSet QueryspCLActivityListCollector()
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = "spCLActivityListCollector";
                //SQLCommand.Parameters.Add("@CGID", SqlDbType.VarChar, 3).Value = HttpContext.Current.Session["groupdbid"].ToString();
                //SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 12).Value = HttpContext.Current.Session["loginid"].ToString();
                //SQLCommand.Parameters.Add("@strKey", SqlDbType.VarChar, 10).Value = "";
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryBranchDue( string strBranchid)
        {
            try
            {
                if (strBranchid == "999")
                {
                    OpenConnection();
                    string strCmd = "select replace(branchid,'CG','') cgid, cgname from openquery(collectionsystem, 'select * from mst_collection_group') order by cgname";
                    SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                    SqlAdapt.Fill(oDataset, "Table");
                }
                else
                {
                    OpenConnection();
                    string strqry = "select cgid,cgname from openquery(collectionsystem, 'select * from mst_collection_group') where  cgid = '" + HttpContext.Current.Session["groupdbid"].ToString() + "'";
                    SqlAdapt = new SqlDataAdapter(strqry, SqlConn);
                    SqlAdapt.Fill(oDataset, "Table");
                }
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryReportspAnjCust_PFCTool(string branchid, string ovd, string collectorid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                SQLCommand.Parameters.Add("@ovd", SqlDbType.VarChar, 10).Value = ovd;
                SQLCommand.Parameters.Add("@collectorid", SqlDbType.VarChar, 10).Value = collectorid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryRptDistAsuransi(string branchid, string tglfrom, string tglto, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.VarChar, 8000).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.VarChar, 8000).Value = tglto;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable QueryReportspSAFCustCallCenterRptProductivityDatabase(string date1, string date2, string areaid, string branchid, string sourceid, string datetype, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 10).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 10).Value = date2;
                base.SQLCommand.Parameters.Add("@areaid", SqlDbType.VarChar, 3).Value = areaid;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                base.SQLCommand.Parameters.Add("@sourceid", SqlDbType.VarChar, 3).Value = sourceid;
                base.SQLCommand.Parameters.Add("@datetype", SqlDbType.VarChar, 1).Value = datetype;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public DataTable QueryReportspSAFCustCallCenterRptClosingList(string date1, string date2, string areaid, string branchid, string datetype, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 10).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 10).Value = date2;
                base.SQLCommand.Parameters.Add("@areaid", SqlDbType.VarChar, 3).Value = areaid;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                base.SQLCommand.Parameters.Add("@datetype", SqlDbType.Int).Value = datetype;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public DataTable QueryReportspSAFCustCallCenterRptSMSInbox(string date1, string date2, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 0x1f40).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 0x1f40).Value = date2;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public System.Data.DataTable QueryReportspMPMFCust_RptDealerMatrix(string tahun, string bulan, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@tahun", SqlDbType.VarChar, 10).Value = tahun;
                SQLCommand.Parameters.Add("@bulan", SqlDbType.VarChar, 10).Value = bulan;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryRekapToInsCo(string branch, string inscoid, string date, string date1, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 3).Value = branch;
                SQLCommand.Parameters.Add("@InsuranceCoyBranchID", SqlDbType.VarChar, 20).Value = inscoid;
                SQLCommand.Parameters.Add("@FromDate", SqlDbType.VarChar, 10).Value = date;
                SQLCommand.Parameters.Add("@EndDate", SqlDbType.VarChar, 10).Value = date1;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryRekapInternalView(string branch, string date, string date1, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 3).Value = branch;
                SQLCommand.Parameters.Add("@FromDate", SqlDbType.VarChar, 10).Value = date;
                SQLCommand.Parameters.Add("@EndDate", SqlDbType.VarChar, 10).Value = date1;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryInsco(string branchid)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = "spMPMFCustInsCoESPPA";
                SQLCommand.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportPaymentSummary(string branch, string reporttype, string assetCode, string periodtypeid, string month, string year, string date1, string date2, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 20).Value = branch;
                SQLCommand.Parameters.Add("@assetCode", SqlDbType.Char, 20).Value = assetCode;
                SQLCommand.Parameters.Add("@periodtypeid", SqlDbType.Char, 20).Value = periodtypeid;
                SQLCommand.Parameters.Add("@reportType", SqlDbType.Char, 20).Value = reporttype;
                SQLCommand.Parameters.Add("@month", SqlDbType.Char, 20).Value = month;
                SQLCommand.Parameters.Add("@year", SqlDbType.Char, 20).Value = year;
                SQLCommand.Parameters.Add("@date1", SqlDbType.Char, 20).Value = date1;
                SQLCommand.Parameters.Add("@date2", SqlDbType.Char, 20).Value = date2;

                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public DataTable QueryRptApTaxFiduciaFee(string branch, string period1, string period2, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 14400;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@period1", SqlDbType.VarChar, 10).Value = period1;
                base.SQLCommand.Parameters.Add("@period2", SqlDbType.VarChar, 10).Value = period2;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branch;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }


        public DataTable QueryRptKlikBCA(string date1, string date2, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 14400;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 10).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 10).Value = date2;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public System.Data.DataSet QueryAgreementCardListSet(string branchid, string cari, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@cari", SqlDbType.VarChar, 8000).Value = cari;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryAmortisasi(string applicationid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("applicationid", SqlDbType.Char, 20).Value = applicationid;

                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportBooking(string branch, string date1, string date2, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@OFFICE_CODE", SqlDbType.Char, 20).Value = branch;
                SQLCommand.Parameters.Add("@date1", SqlDbType.Char, 20).Value = date1;
                SQLCommand.Parameters.Add("@date2", SqlDbType.Char, 20).Value = date2;

                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable QueryReportspANJCustGenARDetailBln(string branchid, string assettypeid, string agingbln, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 14400;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                base.SQLCommand.Parameters.Add("@assettypeid", SqlDbType.VarChar, 3).Value = assettypeid;
                base.SQLCommand.Parameters.Add("@agingbln", SqlDbType.VarChar, 6).Value = agingbln;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public DataTable QueryRptCustAutoAssign(string LoginID, string tglfrom, string tglto, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@LoginID", SqlDbType.VarChar, 0x1f40).Value = LoginID;
                base.SQLCommand.Parameters.Add("@tglfrom", SqlDbType.VarChar, 0x1f40).Value = tglfrom;
                base.SQLCommand.Parameters.Add("@tglto", SqlDbType.VarChar, 0x1f40).Value = tglto;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public System.Data.DataSet QueryCreditAnalyst()
        {
            try
            {
                OpenConnection();
                string strCmd = "SELECT distinct  CAID, LoginID FROM confinsr1.SAFDB.dbo.AutoAssignCAAlocationLog with(nolock) where isautoassign = 1 and caid = loginid";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFCustRptDailyNPL(string Branch, string AgingDate, string Product, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = Branch;
                SQLCommand.Parameters.Add("@agingdate", SqlDbType.VarChar, 8000).Value = AgingDate;
                SQLCommand.Parameters.Add("@productjrn", SqlDbType.VarChar, 8000).Value = Product;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspAnjCustRptAssetRepoGainLoss(string branchid, string periodfrom, string periodto, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@periodfrom", SqlDbType.VarChar, 8000).Value = periodfrom;
                SQLCommand.Parameters.Add("@periodto", SqlDbType.VarChar, 8000).Value = periodto;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryRptLapProduksiAsuransi(string branchid, string reportType, string year, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@reportType", SqlDbType.VarChar, 8000).Value = reportType;
                SQLCommand.Parameters.Add("@year", SqlDbType.VarChar, 8000).Value = year;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public DataTable QueryRptInitial_Interview_Services(string branchid, string batchId, string date1, string date2, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 0x1f40).Value = branchid;
                base.SQLCommand.Parameters.Add("@batchId", SqlDbType.VarChar, 0x1f40).Value = batchId;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 0x1f40).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 0x1f40).Value = date2;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }


        public DataTable QueryReportspMPMFCust_RptUpload_Payment(string branchid, string date1, string date2, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@date1", SqlDbType.VarChar, 0x1f40).Value = date1;
                base.SQLCommand.Parameters.Add("@date2", SqlDbType.VarChar, 0x1f40).Value = date2;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 0x1f40).Value = branchid;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }


        public System.Data.DataTable QueryspMPMFCustRekapLaporanKeluhanPerBulanCS(string BranchID, string Tahun, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 8000).Value = BranchID;
                SQLCommand.Parameters.Add("@Tahun", SqlDbType.VarChar, 8000).Value = Tahun;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryspMPMFCustRekapLaporanKeluhanCS(string BranchID, string FromDate, string EndDate, string status, string problemid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                //SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 8000).Value = "000" ;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 8000).Value = BranchID;
                SQLCommand.Parameters.Add("@FromDate", SqlDbType.VarChar, 8000).Value = FromDate;
                SQLCommand.Parameters.Add("@EndDate", SqlDbType.VarChar, 8000).Value = EndDate;
                SQLCommand.Parameters.Add("@status", SqlDbType.VarChar, 8000).Value = status;
                SQLCommand.Parameters.Add("@problemid", SqlDbType.VarChar, 8000).Value = problemid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryspMPMFCustRptCompPie(string branchid, string startdate, string enddate, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@enddate", SqlDbType.VarChar, 8000).Value = enddate;
                SQLCommand.Parameters.Add("@startdate", SqlDbType.VarChar, 8000).Value = startdate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryspMPMFCustLaporanKeluhanPerMasaCS(string BranchID, string FromDate, string EndDate, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 8000).Value = BranchID;
                SQLCommand.Parameters.Add("@FromDate", SqlDbType.VarChar, 8000).Value = FromDate;
                SQLCommand.Parameters.Add("@EndDate", SqlDbType.VarChar, 8000).Value = EndDate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryspMPMFCustGenARDetail(string branchid, string assettypeid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@assettypeid", SqlDbType.Char, 10).Value = assettypeid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryspMPMFCustGenARDetail(string areaId, string branchid, string assettypeid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.VarChar, 15).Value = areaId;
                SQLCommand.Parameters.Add("@assettypeid", SqlDbType.Char, 10).Value = assettypeid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }





        public System.Data.DataTable QueryReportspAnjCustRptDailyInsurance(string branchid, string tglfrom, string tglto, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.Char, 10).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.Char, 10).Value = tglto;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspAnjCustRptDailyInsurance(string branchid, string tglfrom, string tglto, string status, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.Char, 10).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.Char, 10).Value = tglto;
                SQLCommand.Parameters.Add("@Status", SqlDbType.Char, 20).Value = status;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryRptMPMFCustTerimaByrIns(string branchid, string tglfrom, string tglto, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.Char, 10).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.Char, 10).Value = tglto;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryRptMPMFCustTerimaByrIns(string branchid, string tglfrom, string tglto, string status, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.Char, 10).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.Char, 10).Value = tglto;
                SQLCommand.Parameters.Add("@Status", SqlDbType.Char, 20).Value = status;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public DataSet QueryGetYear()
        {
            DataSet oDataset;
            try
            {
                base.OpenConnection();
                string selectCommandText = "exec spMPMFCust_GetYear";
                base.SqlAdapt = new SqlDataAdapter(selectCommandText, base.SqlConn);
                base.SqlAdapt.Fill(base.oDataset, "Table");
                oDataset = base.oDataset;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return oDataset;
        }


        public DataSet QuerySupplierByBranch(string branchid, string year)
        {
            DataSet oDataset;
            try
            {
                base.OpenConnection();
                string selectCommandText = "select distinct a.suppl_code as supplierid,	a.suppl_name as suppliername from confins..suppl a with(nolock)  inner join (select g.suppl_id from confins..agrmnt b with(nolock)  inner join confins..ref_office c with(nolock) on b.ref_office_id = c.ref_office_id and c.IS_VIRTUAL_OFFICE = 0  inner join	confins..agrmnt_asset d with(nolock) on b.agrmnt_id = d.agrmnt_id  inner join confins..agrmnt_asset_suppl e with(nolock) on d.agrmnt_asset_id = e.agrmnt_asset_id inner join confins..suppl_branch f with(nolock) on e.suppl_branch_id = f.suppl_branch_id inner join confins..suppl g with(nolock) on f.suppl_id = g.suppl_id where c.office_code = '" + branchid + "' and year(b.go_live_dt) = '" + year + "')q1 on q1.suppl_id = a.suppl_id order by suppl_name";
                base.SqlAdapt = new SqlDataAdapter(selectCommandText, base.SqlConn);
                base.SqlAdapt.Fill(base.oDataset, "Table");
                oDataset = base.oDataset;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return oDataset;
        }

        public DataSet spMPMFCust_TaxGrid(string branchid, string marketing, string year, string supplierid, string spname)
        {
            DataSet oDataset;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                base.SQLCommand.Parameters.Add("@marketing", SqlDbType.VarChar, 50).Value = marketing;
                base.SQLCommand.Parameters.Add("@year", SqlDbType.VarChar, 4).Value = year;
                base.SQLCommand.Parameters.Add("@supplierid", SqlDbType.VarChar, 50).Value = supplierid;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                oDataset = base.oDataset;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return oDataset;
        }

        public DataTable QueryMPMFCust_RptTax(string supplierId, string branchId, string yaer, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 14400;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@SupplierKTPId", SqlDbType.VarChar, 30).Value = supplierId;
                base.SQLCommand.Parameters.Add("@branchId", SqlDbType.VarChar, 3).Value = branchId;
                base.SQLCommand.Parameters.Add("@stryear", SqlDbType.VarChar, 4).Value = yaer;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public System.Data.DataTable QueryReportspMPMFCustRptDealerMatrixSummary(string year,string month, string areaCode,string branchCode, string prod, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@year", SqlDbType.Char, 5).Value = year;
                SQLCommand.Parameters.Add("@month", SqlDbType.Char, 5).Value = month;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.Char, 5).Value = areaCode;
                SQLCommand.Parameters.Add("@branchCode", SqlDbType.Char, 5).Value = branchCode;
                SQLCommand.Parameters.Add("@prod", SqlDbType.Char, 5).Value = prod;
                
                
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryReportspMPMFCustRptProductionYear(string branchid, string year, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@year", SqlDbType.Char, 5).Value = year;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFCustRptProductionMonth(string branchid, string year, string month, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.Char, 5).Value = branchid;
                SQLCommand.Parameters.Add("@year", SqlDbType.Char, 5).Value = year;
                SQLCommand.Parameters.Add("@month", SqlDbType.Char, 5).Value = month;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryMPMFCustRptDisburseList(string aptype, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@aptype", SqlDbType.Char, 10).Value = aptype;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryspAnjCustRptTTR(string tglfrom, string tglto, string branchid, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.VarChar, 0).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.VarChar, 0).Value = tglto;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 8000).Value = branchid;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }



        #region Laporan Monitoring
        public System.Data.DataTable QueryReportspMPMFCustRptMonitoringProsesFidusia(string BranchID, string status, string startdate, string enddate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 10).Value = BranchID;
                SQLCommand.Parameters.Add("@status", SqlDbType.VarChar, 10).Value = status;
                SQLCommand.Parameters.Add("@startdate", SqlDbType.VarChar, 10).Value = startdate;
                SQLCommand.Parameters.Add("@enddate", SqlDbType.VarChar, 25).Value = enddate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspMPMFCustRptMonitoringSuratTugas(string BranchID, string startdate, string enddate, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 10).Value = BranchID;
                SQLCommand.Parameters.Add("@startdate", SqlDbType.VarChar, 10).Value = startdate;
                SQLCommand.Parameters.Add("@enddate", SqlDbType.VarChar, 25).Value = enddate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion

        public DataSet QuerySuplier(string strbranchid)
        {
            DataSet oDataset;
            string str = "";
            try
            {
                base.OpenConnection();
                if (strbranchid.ToString() != "ALL")
                {
                    str = "WHERE ro.Office_code = '" + strbranchid.ToString() + "'";
                }
                string selectCommandText = "select distinct s.SUPPL_CODE SupplierID, ro.OFFICE_CODE BranchID, s.SUPPL_NAME SupplierName from CONFINS..SUPPL s with(nolock) inner join CONFINS..SUPPL_BRANCH sb with(nolock) on s.SUPPL_ID = sb.SUPPL_ID left join CONFINS..AGRMNT_ASSET_SUPPL aas with(nolock) on aas.SUPPL_BRANCH_ID = sb.SUPPL_BRANCH_ID left join CONFINS..AGRMNT_ASSET aa with(nolock) on aas.AGRMNT_ASSET_ID = aa.AGRMNT_ASSET_ID left join CONFINS..AGRMNT a with(nolock) on aa.AGRMNT_ID = a.AGRMNT_ID left join CONFINS..REF_OFFICE ro with(nolock) on a.REF_OFFICE_ID = ro.REF_OFFICE_ID ";
                selectCommandText = selectCommandText + str + " order by s.SUPPL_NAME";
                base.SqlAdapt = new SqlDataAdapter(selectCommandText, base.SqlConn);
                base.SqlAdapt.Fill(base.oDataset, "Table");
                oDataset = base.oDataset;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return oDataset;
        }

        public DataTable QueryRptSalesBySupplier(string branchid, string date1, string date2, string supplierid, string assettypeid, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 0).Value = branchid;
                base.SQLCommand.Parameters.Add("@periodfrom", SqlDbType.VarChar, 0).Value = date1;
                base.SQLCommand.Parameters.Add("@periodto", SqlDbType.VarChar, 0).Value = date2;
                base.SQLCommand.Parameters.Add("@supplierid", SqlDbType.VarChar, 0).Value = supplierid;
                //base.SQLCommand.Parameters.Add("@assettypeid", SqlDbType.VarChar, 0x1f40).Value = assettypeid;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public DataTable QueryRptPaymentRequestList(string requestType, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@reqtype", SqlDbType.VarChar, 0).Value = requestType;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }


        public DataTable QueryRptreportARMonthly(string AgingDate, string branch, string Status, string DaysOverdue, string ContractStatus, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@agingdate", SqlDbType.VarChar, 0).Value = AgingDate;
                base.SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 0).Value = branch;
                base.SQLCommand.Parameters.Add("@balance", SqlDbType.VarChar, 0).Value = Status;
                base.SQLCommand.Parameters.Add("@ovd", SqlDbType.VarChar, 0).Value = DaysOverdue;
                base.SQLCommand.Parameters.Add("@wo", SqlDbType.VarChar, 0x1f40).Value = ContractStatus;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }


        public DataTable QueryRptCIS(string date1, string date2, string spname)
        {
            DataTable table;
            try
            {
                base.OpenConnection();
                base.SQLCommand.Connection = base.SqlConn;
                base.SQLCommand.CommandType = CommandType.StoredProcedure;
                base.SQLCommand.CommandTimeout = 0x3840;
                base.SQLCommand.CommandText = spname;
                base.SQLCommand.Parameters.Add("@StartDate", SqlDbType.VarChar, 0).Value = date1;
                base.SQLCommand.Parameters.Add("@EndDate", SqlDbType.VarChar, 0).Value = date2;

                //base.SQLCommand.Parameters.Add("@assettypeid", SqlDbType.VarChar, 0x1f40).Value = assettypeid;
                base.SqlAdapt.SelectCommand = base.SQLCommand;
                base.SqlAdapt.Fill(base.oDataset, "Table");
                table = base.oDataset.Tables["Table"];
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                base.CloseConnection();
            }
            return table;
        }

        public System.Data.DataTable QueryReportPenerimaanARBulanan(string branchid, string startDate, string endDate, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@officeCode", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@PostingDateFrom", SqlDbType.DateTime, 0).Value = DateTime.ParseExact(startDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                SQLCommand.Parameters.Add("@PostingDateTo", SqlDbType.DateTime, 0).Value = DateTime.ParseExact(endDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryRptVwApprovalScheme(string schemeCode, string schemeName, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@SchemeCode", SqlDbType.VarChar, 30).Value = schemeCode;
                SQLCommand.Parameters.Add("@SchemeName", SqlDbType.VarChar, 60).Value = schemeName;

                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportPenerimaanARBulanan(string AreaId, string branchid, string startDate, string endDate, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@officeCode", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.VarChar, 8000).Value = AreaId;
                SQLCommand.Parameters.Add("@PostingDateFrom", SqlDbType.VarChar, 10).Value = startDate;
                SQLCommand.Parameters.Add("@PostingDateTo", SqlDbType.VarChar, 10).Value = endDate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportDaftarPiutang(string AreaId, string branchid, string startDate, string DfStat, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@RefOfficeID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.VarChar, 8000).Value = AreaId;
                SQLCommand.Parameters.Add("@Period", SqlDbType.VarChar, 10).Value = startDate;
                SQLCommand.Parameters.Add("@DefaultStat", SqlDbType.VarChar, 10).Value = DfStat;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryReportDaftarSupplier(string AreaId, string branchid, string DfStat, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@RefOfficeID", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@areaCode", SqlDbType.VarChar, 8000).Value = AreaId;
                SQLCommand.Parameters.Add("@DefaultStat", SqlDbType.VarChar, 10).Value = DfStat;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public System.Data.DataTable QueryReportYTDLossReport(string Year, string Branch, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@Year", SqlDbType.VarChar, 8000).Value = Year;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 8000).Value = Branch;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportDetailContract(string branchid, string startDate, string endDate, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchId", SqlDbType.VarChar, 8000).Value = branchid;
                SQLCommand.Parameters.Add("@DateFrom", SqlDbType.VarChar, 0).Value = startDate;
                SQLCommand.Parameters.Add("@DateEnd", SqlDbType.VarChar, 0).Value = endDate;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryCurrency()
        {
            try
            {
                OpenConnection();
                string strCmd = "select CURR_CODE as CurrencyID, CURR_NAME as Currency from CONFINS..REF_CURR with(nolock) where IS_ACTIVE = 1";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspDailySales(string strWherecond, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@wherecond", SqlDbType.VarChar, 0).Value = strWherecond;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryBranchArea(string strArea)
        {
            try
            {
                OpenConnection();
                string strCmd = @"select office_code as branchid, office_name as branchfullname 
                                from confins..ref_office ro with(nolock) inner join confins..ref_office_area roa with(nolock) 
                                on ro.ref_office_area_id = roa.ref_office_area_id where ro.IS_VIRTUAL_OFFICE=0 
                                and roa.area_code = '" + strArea.Trim() + "' order by office_name";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QueryPOS(string strofficecode)
        {
            try
            {
                OpenConnection();
                string strCmd = "select office_code as branchid, office_name as branchfullname from confins..ref_office where office_code = '" + strofficecode + "'";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspTopSupplierRpt(string strWhereCond, string nTop, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@WhereBy", SqlDbType.VarChar, 0).Value = strWhereCond;
                SQLCommand.Parameters.Add("@NTop", SqlDbType.VarChar, 20).Value = nTop;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataSet QuerySupplier()
        {
            try
            {
                OpenConnection();
                string strCmd = "select suppl_code, suppl_name from confins..suppl with(nolock) order by suppl_name";
                SqlAdapt = new SqlDataAdapter(strCmd, SqlConn);
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportSpSalesReportPeriodic(string branchid, string SupplierID, string Month, string Year, string PosID, string spname)
        {
            try
            {
                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@BranchID", SqlDbType.VarChar, 3).Value = branchid;
                SQLCommand.Parameters.Add("@SupplierID", SqlDbType.VarChar, 20).Value = SupplierID;
                SQLCommand.Parameters.Add("@Month", SqlDbType.Int, 0).Value = Convert.ToInt32(Month);
                SQLCommand.Parameters.Add("@Year", SqlDbType.Int, 0).Value = Convert.ToInt32(Year);
                SQLCommand.Parameters.Add("@POSID", SqlDbType.Char, 3).Value = PosID;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public System.Data.DataTable QueryReportspmpmfDataRekonPajak(string tglfrom, string tglto, string branchid, string jenispajak, string spname)
        {
            try
            {

                OpenConnection();
                SQLCommand.Connection = SqlConn;
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.CommandTimeout = 14400;
                SQLCommand.CommandText = spname;
                SQLCommand.Parameters.Add("@tglfrom", SqlDbType.VarChar, 10).Value = tglfrom;
                SQLCommand.Parameters.Add("@tglto", SqlDbType.VarChar, 10).Value = tglto;
                SQLCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = branchid;
                SQLCommand.Parameters.Add("@jenispajak", SqlDbType.VarChar, 25).Value = jenispajak;
                SqlAdapt.SelectCommand = SQLCommand;
                SqlAdapt.Fill(oDataset, "Table");
                return oDataset.Tables["Table"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }



    }
}