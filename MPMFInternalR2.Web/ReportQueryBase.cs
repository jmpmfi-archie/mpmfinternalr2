﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace MPMFInternalR2.Web
{
    public class ReportQueryBase
    {
        //protected SqlConnection SqlConn; 
        protected SqlDataAdapter SqlAdapt;
        protected SqlCommand SQLCommand;
        protected DataSet oDataset;
        protected DataTable oDatatable;
        protected SqlParameter SQLParam;
        //private ConnectionString oConnection = new ConnectionString(); 
        protected SqlConnection SqlConn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);

        public ReportQueryBase()
        {
            //SqlConn = new SqlConnection(oConnection.GetConnStr());

            SQLCommand = new SqlCommand();
            SqlAdapt = new SqlDataAdapter();
            oDatatable = new DataTable();
            oDataset = new DataSet();
        }

        protected void OpenConnection()
        {
            //if (SqlConn.State == System.Data.ConnectionState.Closed)
            //{ 
            SqlConn.Open();
            //} 
        }

        protected void CloseConnection()
        {
            //if (SqlConn.State == System.Data.ConnectionState.Open)
            //{ 
            SqlConn.Close();
            SqlConn.Dispose();
            //GC.Collect(1);
            //} 
        }
    }
}