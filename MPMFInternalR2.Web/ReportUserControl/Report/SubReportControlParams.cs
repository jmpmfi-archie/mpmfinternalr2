﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPMFInternalR2.Web.ReportUserControl.Report
{
    [Serializable]
    public class SubReportControlParams
    {
        public string SubRptTmpltCode { get { return _subRptTmpltCode; } }
        private string _subRptTmpltCode { get; set; }

        public string[] RefMainParams { get { return _refMainParams; } }
        private string[] _refMainParams { get; set; }

        public SubReportControlParams(string SubRptTmpltCode, params string[] RefMainParams)
        {
            this._subRptTmpltCode = SubRptTmpltCode;
            this._refMainParams = RefMainParams;
        }
    }
}