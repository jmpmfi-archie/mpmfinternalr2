﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MPMFRptAssetManagement.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMFCust_RptAssetManagement.MPMFRptAssetManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="ReportAssetManagement"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Report Asset Management</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Report Asset Management</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Report Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlRptType" runat="server" OnSelectedIndexChanged="ddlRptType_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Value="00" Selected="True">Select One</asp:ListItem>
                                    <asp:ListItem Value="01" >Repo</asp:ListItem>
                                    <asp:ListItem Value="02">Sold</asp:ListItem>
                                    <asp:ListItem Value="03">Stock</asp:ListItem>

                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlProduct" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Area</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlArea" runat="server" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlbranch" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnlSold">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Period Date From</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                    To
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtEnd" CssClass="datepicker"></asp:TextBox>
                                    <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                        Visible="False">* Please check start and end dates</asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlStock" Visible="false">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Repo Date As Of</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStartStock" CssClass="datepicker"></asp:TextBox>
                                    <asp:Label ID="lblwardateStock" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                        Visible="False">* Required field</asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
