﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitFeeLCAssetRecoveryReport.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.VisitFeeLCAssetRecovery.VisitFeeLCAssetRecoveryReport" %>

<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/VisitFeeLCAssetRecovery"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Visit Fee LC Asset Recovery </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />
        </div>

        <div id="dPDCReceive">
            <div class="subSection">
                <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
            </div>
            <div id="dSearchForm">
                <table id="ucReportSearch_tblFixedSearch" class="formTable">
                    <tr>
                        <td class="tdDesc" style="width: 20%;">Area</td>
                        <td class="tdValue" style="width: 30%;">
                            <asp:DropDownList ID="ddlarea" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlarea_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap" width="30%">Branch</td>
                        <td class="tdganjil">
                            <asp:DropDownList ID="ddlbranch" runat="server">
                            </asp:DropDownList>
                            <asp:Label ID="lblwarbranch" runat="server" Font-Names="verdana"
                                Font-Size="X-Small" ForeColor="Red"
                                Style="font-size: 11px; font-family: Verdana, Arial, Tahoma, sans-serif"
                                Visible="False">*) Required Field</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap" width="30%">Period</td>
                        <td class="tdganjil">
                            <asp:DropDownList ID="ddlperiod1" runat="server">
                                <asp:ListItem Value="00">Select One</asp:ListItem>
                                <asp:ListItem Value="01">Januari</asp:ListItem>
                                <asp:ListItem Value="02">Februari</asp:ListItem>
                                <asp:ListItem Value="03">Maret</asp:ListItem>
                                <asp:ListItem Value="04">April</asp:ListItem>
                                <asp:ListItem Value="05">Mei</asp:ListItem>
                                <asp:ListItem Value="06">Juni</asp:ListItem>
                                <asp:ListItem Value="07">Juli</asp:ListItem>
                                <asp:ListItem Value="08">Agustus</asp:ListItem>
                                <asp:ListItem Value="09">September</asp:ListItem>
                                <asp:ListItem Value="10">Oktober</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">Desember</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlperiod2" runat="server">
                                <asp:ListItem Value="00">Select One</asp:ListItem>
                                <asp:ListItem>2012</asp:ListItem>
                                <asp:ListItem>2013</asp:ListItem>
                                <asp:ListItem>2014</asp:ListItem>
                                <asp:ListItem>2015</asp:ListItem>
                                <asp:ListItem>2016</asp:ListItem>
                                <asp:ListItem>2017</asp:ListItem>
                                <asp:ListItem>2018</asp:ListItem>
                                <asp:ListItem>2019</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblwarddlperiod" runat="server" Font-Names="verdana"
                                Font-Size="X-Small" ForeColor="Red"
                                Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                Visible="False">*) Required Field</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap" width="30%">Asset Type</td>
                        <td class="tdganjil">
                            <asp:DropDownList ID="ddlassettype" runat="server">
                            </asp:DropDownList>
                            <asp:Label ID="lblwarddlassettype" runat="server" Font-Names="verdana"
                                Font-Size="X-Small" ForeColor="Red"
                                Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                Visible="False">*) Required Field</asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="formTable">
                    <tr>
                        <td align="right">
                            <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                <ContentTemplate>
                                    <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                        OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                        CausesValidation="false"></asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
