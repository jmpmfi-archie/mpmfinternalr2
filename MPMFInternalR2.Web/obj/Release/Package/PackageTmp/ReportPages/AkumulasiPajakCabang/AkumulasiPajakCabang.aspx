﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AkumulasiPajakCabang.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.AkumulasiPajakCabang.AkumulasiPajakCabang" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/AkumulasiPajakCabang"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Laporan Akumulasi Pajak Cabang</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc">Branch</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlbranch" runat="server" OnSelectedIndexChanged="ddlbranch_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Period</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlyear" runat="server" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlyear" runat="server" Font-Names="verdana"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                    Visible="False">*) Required Field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Supplier</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlsupplier" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Marketing</td>
                            <td class="tdValue">
                                <asp:TextBox runat="server" ID="txtmarketing"></asp:TextBox>
                                <asp:Label ID="lblwarmarketing" runat="server" Font-Names="verdana"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                    Visible="False">*) Required Field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            
            <div id="upGrid">
                <div id="dGridSection" class="form">
                    <div class="subSection">
                        <a href="javascript:ExpandUnexpandMenu('minGrid','dEmpPaging', 'ucToggleGrid_subSectionID')" id="minGrid">[-]</a>
                        <span id="ucToggleGrid_subSectionID">Purchase Order List</span>
                    </div>
                </div>
                <div id="dEmpPaging" style="width: 100%">
                    <div class="pager" align="left">
                        <a id="ucGridHeader_lbRefreshGrid" href="javascript:__doPostBack('ucGridHeader$lbRefreshGrid','')">Refresh Grid</a>
                    </div>
                    <asp:GridView runat="server" ID="gvRefTableTax" AutoGenerateColumns="False" GridLines="None"
                        CssClass="mGrid" AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="True"
                        EmptyDataText="No records Found" PageSize="10" AllowPaging="True" OnPageIndexChanging="gvRefTableTax_PageIndexChanging"
                        OnRowCommand="gvRefTableTax_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Supplier KTP ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="lblSupplierEmployeeid" Value='<%# Eval("SupplierEmployeeid") %>' />
                                    <asp:Label runat="server" ID="lblsupplierktpid" Text='<%# Eval("supplierktpid") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Supplier Employee Name" HeaderStyle-Width="100" ItemStyle-Width="300" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblName" Text='<%# Eval("supplieremployeename") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal runat="server" ID="ltl_Grid_Action" Text="Action" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnPrint" runat="server" CommandName="Print" Text="Print"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
