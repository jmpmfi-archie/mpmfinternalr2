﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vLaporanMonitoringTTR.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.Monitoring.vLaporanMonitoringTTR" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False"></asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <rsweb:ReportViewer ID="ReportViewer1" Height="800px" Width="100%" runat="server"></rsweb:ReportViewer>
        </div>
        <asp:Label ID="lbltglfrom" runat="server" Visible="false" />
        <asp:Label ID="lbltglto" runat="server" Visible="false" />
        <asp:Label ID="lblpilihan" runat="server" Visible="false" />
        <asp:Label ID="lblpilihn" runat="server" Visible="false" />
        <asp:Label ID="lblreport" runat="server" Visible="false" />
        <asp:Label ID="txtreport" runat="server" Visible="false" />
        <asp:Label ID="lblstatusid" runat="server" Visible="false" />
        <asp:Label ID="lblstatusname" runat="server" Visible="false" />
        <asp:Label ID="lblchoose" runat="server" Visible="false" />
    </form>
</body>
</html>
