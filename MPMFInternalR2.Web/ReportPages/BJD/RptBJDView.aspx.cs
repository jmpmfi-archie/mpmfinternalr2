﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.BJD
{
    public partial class RptBJDView : System.Web.UI.Page
    {
        private string branch, branchname, date, date1, reportType, cutoff, assettypeid, assettype, loginid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchId"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["period"].ToString().Trim();
            date1 = Request.QueryString["period1"].ToString().Trim();
            cutoff = Request.QueryString["cutoff"].ToString().Trim();
            reportType = Request.QueryString["reportType"].ToString().Trim();
            assettypeid = Request.QueryString["assettypeid"].ToString().Trim();
            assettype = Request.QueryString["assettype"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            if (reportType == "In")
            {
                DataTable dt = new DataTable();
                QueryConnection qryin = new QueryConnection();

                dt = qryin.QueryReportBJD(branch, date, date1, reportType, cutoff, "spSAFBJDIn", assettypeid);

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBJDIn.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("branchname", branchname);
                ReportParameter p2 = new ReportParameter("date", date);
                ReportParameter p3 = new ReportParameter("date1", date1);
                ReportParameter p4 = new ReportParameter("reportType", reportType);
                ReportParameter p5 = new ReportParameter("loginid", loginid);
                ReportParameter p6 = new ReportParameter("cutoff", cutoff);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
            else if (reportType == "Sold")
            {
                DataTable dt = new DataTable();
                QueryConnection qryin = new QueryConnection();

                dt = qryin.QueryReportBJD(branch, date, date1, reportType, cutoff, "spSAFBJDSold", assettypeid);

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBJDSold.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("branchname", branchname);
                ReportParameter p2 = new ReportParameter("date", date);
                ReportParameter p3 = new ReportParameter("date1", date1);
                ReportParameter p4 = new ReportParameter("reportType", reportType);
                ReportParameter p5 = new ReportParameter("loginid", loginid);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
            else if (reportType == "ContinueCredit")
            {
                DataTable dt = new DataTable();
                QueryConnection qryin = new QueryConnection();

                dt = qryin.QueryReportBJD(branch, date, date1, reportType, cutoff, "spSAFBJDContinueCredit", assettypeid);

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBJDContinueCredit.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("branchname", branchname);
                ReportParameter p2 = new ReportParameter("date", date);
                ReportParameter p3 = new ReportParameter("date1", date1);
                ReportParameter p4 = new ReportParameter("reportType", reportType);
                ReportParameter p5 = new ReportParameter("loginid", loginid);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
            else
            {
                DataTable dt = new DataTable();
                QueryConnection qryin = new QueryConnection();

                dt = qryin.QueryReportBJD(branch, date, date1, reportType, cutoff, "spSAFBJDStockOld", assettypeid);

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBJDStock.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("branchname", branchname);
                ReportParameter p2 = new ReportParameter("date", date);
                ReportParameter p3 = new ReportParameter("date1", date1);
                ReportParameter p4 = new ReportParameter("reportType", reportType);
                ReportParameter p5 = new ReportParameter("loginid", loginid);
                ReportParameter p6 = new ReportParameter("cutoff", cutoff);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }

            GenerateToExcel(ReportViewer1);
        }

        private void GenerateToExcel(ReportViewer reportViewer1)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=RDLC." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}