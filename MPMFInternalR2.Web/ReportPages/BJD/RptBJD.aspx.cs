﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.BJD
{
    public partial class RptBJD : WebFormBase
    //public partial class RptBJD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //HttpContext.Current.Session["sesBranchId"] = "999";
                //HttpContext.Current.Session["loginid"] = "NikoP";
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
            loadassettype();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadassettype()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlassettype.DataSource = ds;
            ddlassettype.DataTextField = "description";
            ddlassettype.DataValueField = "assettypeid";
            ddlassettype.DataBind();
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}
            ddlassettype.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReportType.SelectedValue.ToString() == "Stock")
            {
                panelcutoff.Visible = true;
                paneldate.Visible = false;
            }
            else
            {
                panelcutoff.Visible = false;
                paneldate.Visible = true;
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptBJD.aspx");
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";
            string datepicker2 = "";

            if (dtStart.Text == "")
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker = dtStart.Text;

            if (dtEnd.Text == "")
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker1 = dtEnd.Text;

            if (dtcutoff.Text == "")
                datepicker2 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker2 = dtcutoff.Text;

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("RptBJDView.aspx?optionreport=" + "&branchId=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&period=" + datepicker.ToString() + "&period1=" + datepicker1.ToString() + "" + "&reportType=" + ddlReportType.SelectedValue.ToString().Trim() + "&cutoff=" + datepicker2.ToString() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "&assettype=" + ddlassettype.SelectedItem.ToString().Trim() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
        }
    }
}