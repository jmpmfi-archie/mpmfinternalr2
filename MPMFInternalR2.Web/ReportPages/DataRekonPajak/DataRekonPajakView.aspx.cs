﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.DataRekonPajak
{
    public partial class DataRekonPajakView : System.Web.UI.Page
    {
        private string branchid, branchfullname, tglfrom, tglto, jenisPajak;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchfullname = Request.QueryString["branchname"].ToString().Trim();
            tglfrom = Request.QueryString["tglfrom"].ToString().Trim();
            tglto = Request.QueryString["tglto"].ToString().Trim();
            jenisPajak = Request.QueryString["JenisPajak"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspmpmfDataRekonPajak(tglfrom, tglto, branchid, jenisPajak, "spmpmfDataRekonPajak");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDataJenisPajak.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("date", tglfrom);
            ReportParameter p2 = new ReportParameter("date1", tglto);
            ReportParameter p3 = new ReportParameter("Branch", branchfullname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}