﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MPMFInternalR2.Web.ReportPages.DashboardComplaintSystem
{
    public partial class DashboardComplaintSystemView : System.Web.UI.Page
    {
        private string branchid, startdate, enddate, areaid, branch;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            startdate = Request.QueryString["startdate"].ToString().Trim();
            enddate = Request.QueryString["enddate"].ToString().Trim();
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branch = Request.QueryString["branchfullname"].ToString().Trim();
            areaid = Request.QueryString["AreaID"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryspMPMFCustRptCompPie(branchid, startdate, enddate, "spMPMFCustRptCompPie");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/COMRptKeluhanPie.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", branchid);
            ReportParameter p2 = new ReportParameter("startdate", startdate);
            ReportParameter p3 = new ReportParameter("enddate", enddate);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}