﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.DashboardComplaintSystem
{
    public partial class DashboardComplaintSystem : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";
                loadData();
            }
        }

        protected void loadData()
        {
            loadddlarea();
            loadbranch();
        }

        protected void ddloffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (ddloffice.SelectedValue.ToString() == "regional")
            {
                regional.Visible = true;
                return;
            }
            else
            {
                regional.Visible = false;
            }
            if (ddloffice.SelectedValue.ToString() == "cabang")
            {
                cabang.Visible = true;
                return;
            }
            else
            {
                cabang.Visible = false;
            }
        }

        private void loadddlarea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlarea.DataSource = ds;
            ddlarea.DataTextField = "areafullname";
            ddlarea.DataValueField = "AreaID";
            ddlarea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlarea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlarea.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
           
            if (ddloffice.SelectedValue =="00")
            {
                lblwaroffice.Visible = true;
                return; 
            }
            else
            {
                lblwaroffice.Visible = false;
            }

            string datepicker = "";
            string datepicker1 = "";

            if (dtStart.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = dtStart.Text;
            }

            if (dtEnd.Text == "")
            {
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker1 = dtEnd.Text;
            }

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("DashboardComplaintSystemView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchfullname=" + ddlbranch.SelectedValue.ToString().Trim() + "&AreaID=" + ddlarea.SelectedValue.ToString().Trim() + "&areafullname=" + ddlarea.SelectedItem.ToString().Trim() +"&startdate="+ datepicker.ToString().Trim() +"&enddate=" + datepicker1.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("DashboardComplaintSystem.aspx");
        }
    }
}