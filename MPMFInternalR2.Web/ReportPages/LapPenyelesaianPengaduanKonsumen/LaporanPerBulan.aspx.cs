﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.LapPenyelesaianPengaduanKonsumen
{
    public partial class LaporanPerBulan :WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";
            }
        }


        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlyear.SelectedValue.ToString().Trim() == "00")
            {
                lblwarddlyear.Visible = true;
                return;
            }
            else
            {
                lblwarddlyear.Visible = false;
            }

            Response.Redirect("LaporanPerBulanView.aspx?optionreport=" + "&Tahun=" + ddlyear.SelectedValue.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("LaporanPerBulan.aspx");
        }

    }
}