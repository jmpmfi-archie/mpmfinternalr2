﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_ReportCIS
{

    public partial class MPMFCust_ReportCISView : System.Web.UI.Page
    {
        private string optionreport, date1, date2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            this.optionreport = base.Request.QueryString["optionreport"].ToString().Trim();

            this.date1 = base.Request.QueryString["date1"].ToString().Trim();
            this.date2 = base.Request.QueryString["date2"].ToString().Trim();


            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (optionreport == "cis")
            {
                dt = qry.QueryRptCIS(date1, date2, "spMPMFCust_CIS");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/rptReportCIS.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("startDate", date1);
                ReportParameter p2 = new ReportParameter("endDate", date2);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
            else
            { dt = qry.QueryRptCIS(date1, date2, "spMPMFCust_CISSelisih");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/rptReportCISSelisih.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("startDate", date1);
                ReportParameter p2 = new ReportParameter("endDate", date2);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

            }

            
        }
    }
}