﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.ReportProductivityDatabase
{
    public partial class CallCenterRptProductivityDatabase : WebFormBase
    //public partial class CallCenterRptProductivityDatabase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.IsPostBack)
            {
                //HttpContext.Current.Session["sesBranchId"] = "999";
                //HttpContext.Current.Session["loginid"] = "NikoP";
                this.loadarea();
            }

        }

        private void loadarea()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryAllArea();
            this.ddlarea.DataSource = set;
            this.ddlarea.DataTextField = "ccareaname";
            this.ddlarea.DataValueField = "ccareaid";
            this.ddlarea.DataBind();
            this.ddlarea.Items.Insert(0, "All");
            this.ddlarea.SelectedIndex = 0;

        }

        protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.loadbranch();
        }

        private void loadbranch()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryAllBranchWhereArea(this.ddlarea.SelectedValue.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "BranchFullName";
            this.ddlbranch.DataValueField = "BranchID";
            this.ddlbranch.DataBind();
            this.ddlbranch.Items.Insert(0, "All");
            this.ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string str = "";
            string str2 = "";

            if (dtStart.Text == "")
                str = DateTime.Now.ToString("dd/MM/yyyy");
            else
                str = dtStart.Text;

            if (dtEnd.Text == "")
                str2 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                str2 = dtEnd.Text;

            if (DateTime.ParseExact(str, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(str2, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("CallCenterRptProductivityDatabaseView.aspx?date1=" + str.ToString().Trim() + "&date2=" + str2.ToString().Trim() + "&areaid=" + this.ddlarea.SelectedValue.ToString().ToString().Trim() + "&areaname=" + this.ddlarea.SelectedItem.Text.ToString().Trim() + "&branchid=" + this.ddlbranch.SelectedValue.ToString().Trim() + "&branchfullname=" + this.ddlbranch.SelectedItem.Text.ToString().Trim() + "&sourceid=" + this.ddlsource.SelectedValue.ToString().Trim() + "&sourcename=" + this.ddlsource.SelectedItem.Text.ToString().Trim() + "&datetype=" + this.ddldatetype.SelectedValue.ToString().Trim() + "&datetypename=" + this.ddldatetype.SelectedItem.Text.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("CallCenterRptProductivityDatabase.aspx");
        }
    }
}