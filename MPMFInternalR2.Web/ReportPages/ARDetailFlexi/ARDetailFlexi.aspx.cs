﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace MPMFInternalR2.Web.ReportPages.ARDetailFlexi
{
    public partial class ARDetailFlexi : WebFormBase
    //public partial class ARDetailFlexi : System.Web.UI.Page

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
            loadassettype();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void loadassettype()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlassettype.DataSource = ds;
            ddlassettype.DataTextField = "description";
            ddlassettype.DataValueField = "assettypeid";
            ddlassettype.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlassettype.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            Response.Redirect("ARDetailFlexiView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchfullname=" + ddlbranch.SelectedItem.ToString().Trim() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ARDetailFlexi.aspx");
        }
    }
}