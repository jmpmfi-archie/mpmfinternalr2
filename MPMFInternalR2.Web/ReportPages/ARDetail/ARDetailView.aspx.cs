﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace MPMFInternalR2.Web.ReportPages.ARDetail
{
    public partial class ARDetailView : System.Web.UI.Page
    {
        private string branchid, assettypeid, AreaId, Areafullname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            assettypeid = Request.QueryString["assettypeid"].ToString().Trim();
            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            //dt = qry.QueryspMPMFCustGenARDetail(branchid, assettypeid, "spMPMFCustGenARDetail");
            dt = qry.QueryspMPMFCustGenARDetail(AreaId,branchid, assettypeid, "uspMPMFCustGenARDetail");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptARDetail.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", branchid);
            ReportParameter p2 = new ReportParameter("assettypeid", assettypeid);
            
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }

    }
}