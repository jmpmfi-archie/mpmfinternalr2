﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace MPMFInternalR2.Web.ReportPages.ARDetail
{
    public partial class ARDetail : WebFormBase
    //public partial class ARDetail : System.Web.UI.Page

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadArea();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //loadbranch();
            loadassettype();
        }

        //protected void loadbranch()
        //{
        //    DataSet ds = new DataSet();
        //    QueryConnection qry = new QueryConnection();
        //    ds = qry.QueryBranch();
        //    ddlbranch.DataSource = ds;
        //    ddlbranch.DataTextField = "branchfullname";
        //    ddlbranch.DataValueField = "branchid";
        //    ddlbranch.DataBind();
        //    if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
        //    {
        //        ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
        //    }
        //    ddlbranch.SelectedIndex = 0;
        //}

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }


        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }


        protected void loadassettype()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlassettype.DataSource = ds;
            ddlassettype.DataTextField = "description";
            ddlassettype.DataValueField = "assettypeid";
            ddlassettype.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlassettype.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            Response.Redirect("ARDetailView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchfullname=" + ddlbranch.SelectedItem.ToString().Trim() 
                + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim()
                 + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim()
                 );
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ARDetail.aspx");
        }
    }
}