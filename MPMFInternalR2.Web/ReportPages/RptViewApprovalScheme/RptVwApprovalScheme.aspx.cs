﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.RptViewApprovalScheme
{
    //public partial class RptVwApprovalScheme : System.Web.UI.Page
    public partial class RptVwApprovalScheme : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!this.IsPostBack)
            {
                //this.fillcbo();
                loadSchemaCode();
                ddlSchemeName.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        private void fillcbo()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlSchemeName.DataSource = ds;
            ddlSchemeName.DataTextField = "OFFICE_NAME";
            ddlSchemeName.DataValueField = "OFFICE_CODE";
            ddlSchemeName.DataBind();
            ddlSchemeName.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlSchemeName.SelectedIndex = 0;

            ds.Dispose();
        }


        private void loadSchemaCode()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QuerySchemaCode();
            ddlSchemeCode.DataSource = ds;
            ddlSchemeCode.DataTextField = "SCHEME_CODE";
            ddlSchemeCode.DataValueField = "SCHEME_CODE";
            ddlSchemeCode.DataBind();
            ddlSchemeCode.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlSchemeCode.SelectedIndex = 0;
        }

        private void LoadSchemaName(string schemaCode)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QuerySchemaCode(schemaCode);
            ddlSchemeName.DataSource = ds;
            ddlSchemeName.DataTextField = "SCHEME_NAME";
            ddlSchemeName.DataValueField = "SCHEME_NAME";
            ddlSchemeName.DataBind();
            ddlSchemeName.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlSchemeName.SelectedIndex = 0;
        }


        protected void ddlSchemeCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSchemaName(ddlSchemeCode.SelectedValue.Trim());
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            //if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
            //{
            //    this.lblwardate.Text = "Please Fill Period From And Period To";
            //    lblwardate.Visible = true; return;
            //}

            //if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            //{
            //    lblwardate.Visible = true; return;
            //}
            //else
            //{
            //    lblwardate.Visible = false;
            //}

            Response.Redirect("RptVwApprovalSchemeViewer.aspx?optionreport=" + "&SchemeCode=" + ddlSchemeCode.SelectedValue.ToString().Trim()
                + "&SchemeName=" + ddlSchemeName.SelectedValue.ToString().Trim()
                //+ "&startDate=" + dtStart.Text.ToString()
                //+ "&endDate=" + dtEnd.Text.ToString()
                //+ "&AreaId=" + ddlSchemaCode.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlSchemaCode.SelectedItem.Text.Trim()
                );
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptVwApprovalScheme.aspx");
        }
    }
}