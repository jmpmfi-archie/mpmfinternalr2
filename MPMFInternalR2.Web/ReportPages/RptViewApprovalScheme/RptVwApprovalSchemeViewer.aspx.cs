﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RptViewApprovalScheme
{
    public partial class RptVwApprovalSchemeViewer : System.Web.UI.Page
    {
        private string schemeCode, schemeName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            schemeCode = Request.QueryString["SchemeCode"].ToString().Trim();
            schemeName = Request.QueryString["SchemeName"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryRptVwApprovalScheme(schemeCode, schemeName, "uspMPMFRptVwApprovalScheme");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptVwApprovalScheme.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("SchemeCode", schemeCode);
            ReportParameter p2 = new ReportParameter("SchemeName", schemeName);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}