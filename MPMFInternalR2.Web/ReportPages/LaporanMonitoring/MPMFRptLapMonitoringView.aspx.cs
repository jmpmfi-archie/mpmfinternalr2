﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.LaporanMonitoring
{
    public partial class MPMFRptLapMonitoringView : System.Web.UI.Page
    {
        private string branchid, branchfullname, RptType, startDate, endDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchfullname = Request.QueryString["branchname"].ToString().Trim();
            RptType = Request.QueryString["RptType"].ToString().Trim();
            startDate = Request.QueryString["StartDate"].ToString().Trim();
            endDate = Request.QueryString["EndDate"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (RptType.ToString() == "01")
            {
                dt = qry.QueryReportspMPMFAssetManagement(branchid, startDate, endDate,  "spMPMFAmRepo");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptAssetMgmntRepo.rdlc");
            }
            else if (RptType.ToString() == "02")
            {
                dt = qry.QueryReportspMPMFAssetManagement(branchid, startDate, endDate,  "spMPMFAmSold");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptAssetMgmntSold.rdlc");
            }
            //else if (RptType.ToString() == "03")
            //{
            //    dt = qry.QueryReportspMPMFAssetManagement(branchid, startDate,endDate,  "spMPMFAmStock");
            //    ReportViewer1.ProcessingMode = ProcessingMode.Local;
            //    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptAssetMgmntStock.rdlc");
            //}

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("date", startDate);
            ReportParameter p2 = new ReportParameter("date1", endDate);
            ReportParameter p3 = new ReportParameter("Branch", branchfullname);
            //ReportParameter p4 = new ReportParameter("assettype", RptType);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}