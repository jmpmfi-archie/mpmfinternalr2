﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.FID
{
    //public partial class RptFIDEloan : System.Web.UI.Page
    public partial class RptFIDEloan : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["loginid"] = "NikoP";
            //Session["sesBranchId"] = "999";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {

            loadbranch();

        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("All", "All"));
            }
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";

            if (dtStart.Text == "")
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker = dtStart.Text;

            Response.Redirect("RptFIDEloanView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&assetCode=" + ddlAsset.SelectedValue.ToString().Trim() + "&assetName=" + ddlAsset.SelectedItem.Text.ToString().Trim() + "&reportType=" + ddlReport.SelectedValue.ToString().Trim() + "&reportName=" + ddlReport.SelectedItem.ToString().Trim() + "&period=" + datepicker.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptFIDEloan.aspx");
        }
    }
}