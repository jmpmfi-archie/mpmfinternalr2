﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.PObyEmail
{
    public partial class RekapInternalView : System.Web.UI.Page
    {
        private string branch, date, date1 ;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            date = base.Request.QueryString["tglfrom"];
            date1 = base.Request.QueryString["tglto"];
            branch = base.Request.QueryString["branchid"];

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRekapInternalView(branch,date, date1, "spMPMFCustPORptRekapInternal");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RkpInternal.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("From", date);
            ReportParameter p2 = new ReportParameter("End", date1);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}