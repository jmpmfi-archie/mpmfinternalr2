﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCustDaftarSupplier
{
    public partial class DaftarSupplierViewer : System.Web.UI.Page
    {
        private string branch, branchname,  dfStat, AreaId, Areafullname, dfStatNm;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchId"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();

            dfStat = Request.QueryString["DfStat"].ToString().Trim();
            dfStatNm = Request.QueryString["DfStatNm"].ToString().Trim();

            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportPenerimaanARBulanan(branch, startDate, endDate, "spPenerimaanARBulanan");
            dt = qry.QueryReportDaftarSupplier(AreaId, branch, dfStat, "uspMPMFReportDaftarSupplier");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/DaftarSupplier.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("CoyName", "PT JACCS MPM Finance Indonesia");
            ReportParameter p2 = new ReportParameter("OfficeName", branchname);
            ReportParameter p3 = new ReportParameter("Status", dfStatNm);
            ReportParameter p4 = new ReportParameter("AreaName", Areafullname);

            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}