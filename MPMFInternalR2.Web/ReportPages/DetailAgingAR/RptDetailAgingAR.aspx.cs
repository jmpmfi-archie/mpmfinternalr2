﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.DetailAgingAR
{
    public partial class RptDetailAgingAR : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";

            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadcategory();
            ddlbrand.Items.Insert(0, new ListItem("ALL", "ALL"));
            loadbranch();

        }

        private void loadcategory()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCategory();
            ddlcategory.DataSource = ds;
            ddlcategory.DataTextField = "descriptionproduct";
            ddlcategory.DataValueField = "initialproductcode";
            ddlcategory.DataBind();
            ddlcategory.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlcategory.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {

                ddlbranch.Items.Insert(0, new ListItem("All", "All"));
            }
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            string WhereCond = "InitialProductCode='" + ddlcategory.SelectedValue.ToString() + "'";

            ds = qry.QueryFID(WhereCond, "spBrandCategory_Vera");
            ddlbrand.DataSource = ds;
            ddlbrand.DataTextField = "AssetCode";
            ddlbrand.DataValueField = "AssetCode";
            ddlbrand.DataBind();
            ddlbrand.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbrand.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            if (dtStart.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = dtStart.Text;
            }

            Response.Redirect("RptDetailAgingView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&categoryID=" + ddlcategory.SelectedValue.ToString().Trim() + "&categoryname=" + ddlcategory.SelectedItem.ToString().Trim() + "&brand=" + ddlbrand.SelectedValue.ToString().Trim() + "&brandname=" + ddlbrand.SelectedItem.ToString().Trim() + "&period=" + datepicker.ToString() + "&paid=" + ddlPaid.SelectedValue.ToString().Trim() + "&status=" + ddlStatus.SelectedValue.ToString().Trim() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptDetailAgingAR.aspx");
        }
    }
}