﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.VisitFeeLCAssetRecovery
{
    public partial class VisitFeeLCAssetRecoveryReportView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Tambahan AdIns untuk Report Beda Pool
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        protected void bindreport()
        {
            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportspSAFVisitFeeLCAssetRecovery(Request.QueryString["branchid"], Request.QueryString["period2"] + Request.QueryString["period1"].ToString(), Request.QueryString["assetid"].ToString(), "spMPMFCustRptVisitLCandAssetRecovery");

            //begin parameter
            //disvalue.Value = Request.QueryString["branchname"].ToString();
            //disvalue1.Value = Request.QueryString["period1name"].ToString();
            //disvalue2.Value = Request.QueryString["period2name"].ToString();
            //disvalue3.Value = Request.QueryString["assettypename"].ToString();
            //pfield.Name = "date1";
            //pfield1.Name = "date2";
            //pfield2.Name = "branchfullname";
            //pfield3.Name = "datetypename";
            //pfield.CurrentValues.Add(disvalue);
            //pfield1.CurrentValues.Add(disvalue1);
            //pfield2.CurrentValues.Add(disvalue2);
            //pfield3.CurrentValues.Add(disvalue3);
            //pfields.Add(pfield);
            //pfields.Add(pfield1);
            //pfields.Add(pfield2);
            //pfields.Add(pfield3);
            //CRViewerVisitFeeLCAssetRecovery.ParameterFieldInfo = pfields;
            //end parameter 
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptVisitFeeLCAssetRecovery.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}