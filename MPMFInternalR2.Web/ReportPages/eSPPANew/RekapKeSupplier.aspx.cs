﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.eSPPANew
{
    public partial class RekapKeSupplier : WebFormBase
    //public partial class RekapKeSupplier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "NikoP";
                loadData();
            }
        }

        private void loadData()
        {
            loadInsco();
        }

        private void loadInsco()
        {
            var branchid = base.CurrentUserContext.OfficeCode.ToString();
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryInsco(branchid);
            ddlinsco.DataSource = ds;
            ddlinsco.DataTextField = "InsuranceCoyBranchName";
            ddlinsco.DataValueField = "InsuranceCoyBranchId";
            ddlinsco.DataBind();
            ddlinsco.Items.Insert(0, new ListItem("Choose One", "000"));
            ddlinsco.SelectedIndex = this.ddlinsco.Items.IndexOf(this.ddlinsco.Items.FindByText("Choose One"));
            
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";
            string datepicker2 = "";

            if (dtStart.Text == "")
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker = dtStart.Text;

            if (dtEnd.Text == "")
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker1 = dtEnd.Text;

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            if (ddlinsco.SelectedValue != "ALL")
            {
                Response.Redirect("RekapKeSupplierView.aspx?tglfrom=" + datepicker + "&tglto=" + datepicker1 + " &inscoid=" + this.ddlinsco.SelectedValue.ToString().Trim() + " &branchid=" + base.CurrentUserContext.OfficeCode.ToString() + "");
                lblwarinsco.Visible = false;
            }
            else
            {
                lblwarinsco.Visible = true;
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RekapKeSupplier.aspx");
        }
    }
}