﻿using Microsoft.Reporting.WebForms;
using MPMFInternalR2.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages
{
    public partial class RptFiduciaListView : System.Web.UI.Page
    {
        private string branch, branchname, date, date1, loginid, notaryid, notaryname;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)

            {
                branch = Request.QueryString["branchID"].ToString().Trim();
                branchname = Request.QueryString["branchname"].ToString().Trim();
                date = Request.QueryString["period"].ToString().Trim();
                date1 = Request.QueryString["period1"].ToString().Trim();
                notaryid = Request.QueryString["notaryid"].ToString().Trim();
                notaryname = Request.QueryString["notaryname"].ToString().Trim();

                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFiduciaList.rdlc");

                DataSet ds = GetReport();

                ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportParameter p1 = new ReportParameter("branchname", branchname);
                ReportParameter p2 = new ReportParameter("startDate", date);
                ReportParameter p3 = new ReportParameter("endDate", date1);
                ReportParameter p4 = new ReportParameter("notaryname", notaryname);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4});
                ReportViewer1.LocalReport.DataSources.Add(datasource);

            }
        }

        private DataSet GetReport()
        {
            branch = Request.QueryString["branchID"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["period"].ToString().Trim();
            date1 = Request.QueryString["period1"].ToString().Trim();
            notaryid = Request.QueryString["notaryid"].ToString().Trim();
            notaryname = Request.QueryString["notaryname"].ToString().Trim();

            string conString = ConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 14400;
            cmd.CommandText = "spSAFFiduciaList";
            cmd.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branch;
            cmd.Parameters.Add("@startDate", SqlDbType.VarChar, 8000).Value = date;
            cmd.Parameters.Add("@endDate", SqlDbType.VarChar, 8000).Value = date1;
            cmd.Parameters.Add("@notaryid", SqlDbType.VarChar, 8000).Value = notaryid;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    sda.Fill(ds, "DataTable1");
                    return ds;
                }
            }
        }
    }
}