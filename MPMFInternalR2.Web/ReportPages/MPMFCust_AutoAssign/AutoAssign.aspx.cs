﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_AutoAssign
{
    public partial class AutoAssign : WebFormBase
    //public partial class AutoAssign : System.Web.UI.Page

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadanalyst();
        }

        protected void loadanalyst()
        {
            DataSet ds = new DataSet();

            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCreditAnalyst();
            ddlanalyst.DataSource = ds;
            ddlanalyst.DataTextField = "LoginID";
            ddlanalyst.DataValueField = "LoginID";
            ddlanalyst.DataBind();


            ddlanalyst.Items.Insert(0, new ListItem("Select One", "000"));
           // ddlanalyst.Items.Insert(1, new ListItem("All", "All"));
            ddlanalyst.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {

            string datepicker = "";
            string datepicker1 = "";

            if (dtStart.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = dtStart.Text;
            }

            if (dtEnd.Text == "")
            {
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker1 = dtEnd.Text;
            }

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("AutoAssignView.aspx?optionreport=" + "&LoginID="+ ddlanalyst.SelectedValue.ToString().Trim() + "&tglfrom=" + datepicker.ToString().Trim() + "&tglto=" + datepicker1.ToString().Trim() + "");
        }


        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("AutoAssign.aspx");
        }

    }
}