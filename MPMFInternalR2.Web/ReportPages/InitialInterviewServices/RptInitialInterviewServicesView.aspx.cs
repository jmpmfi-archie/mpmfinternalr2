﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MPMFInternalR2.Web.ReportPages.InitialInterviewServices
{
    public partial class RptInitialInterviewServicesView : System.Web.UI.Page
    {
        private string batchId, branchid, date1, date2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        protected void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            batchId = Request.QueryString["batchId"].ToString().Trim();
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();

            DataTable dataTable = new DataTable();
            dataTable = new QueryConnection().QueryRptInitial_Interview_Services(branchid,batchId,date1,date2, "spMPMFCustRptInitialInterviewServices");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptInitialInterviewServicesTest.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportParameter p1 = new ReportParameter("branchid", branchid);
            ReportParameter p2 = new ReportParameter("batchId", batchId);
            ReportParameter p3 = new ReportParameter("date1", date1);
            ReportParameter p4 = new ReportParameter("date2", date2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 ,p4});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }

    }
}