﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportANJF_ANJCust
{
    //public partial class ReportANJF_ARMonthly : System.Web.UI.Page
    public partial class ReportANJF_ARMonthly : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";

            if (!IsPostBack)
            {
                loadBranch();
            }
        }

        private void loadBranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (date1.Text == "")
            {
                lbldate1.Visible = true;
            }
            else
            {
                Response.Redirect("vrptReportANJF_ARMonthly.aspx?sesARMAgingDate=" + date1.Text + "&sesARMBranch=" + ddlbranch.SelectedValue.ToString().Trim() + "&sesARMStatus=" + cbostatus.SelectedValue.ToString().Trim() + "&sesARMDaysOverdue=" + cbodays.SelectedValue.ToString().Trim() + "&sesARMContractStatus=" + cbocontract.SelectedValue.ToString() + "");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportANJF_ARMonthly.aspx");
        }
    }
}