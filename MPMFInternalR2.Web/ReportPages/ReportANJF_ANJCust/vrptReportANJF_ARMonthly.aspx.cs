﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportANJF_ANJCust
{
    public partial class vrptReportANJF_ARMonthly : System.Web.UI.Page
    {
        private string AgingDate, branch, Status, DaysOverdue, ContractStatus;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadReport();
            }
        }

        private void LoadReport()
        {
            AgingDate = Request.QueryString["sesARMAgingDate"].ToString().Trim(); 
            branch = Request.QueryString["sesARMBranch"].ToString().Trim(); 
            Status = Request.QueryString["sesARMStatus"].ToString().Trim(); 
            DaysOverdue = Request.QueryString["sesARMDaysOverdue"].ToString().Trim(); 
            ContractStatus = Request.QueryString["sesARMContractStatus"].ToString().Trim(); 

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRptreportARMonthly(AgingDate, branch, Status, DaysOverdue, ContractStatus, "spAnjCustrptreportARMonthly");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report_ReportANJF_ARMonthly.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("AgingDate", AgingDate);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}