﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MPMFInternalR2.Web.ReportPages.ReportPayment
{
    public partial class RptReportPaymentView : System.Web.UI.Page
    {
        private string branch, branchname, year, month, date1, date2, assetCode, reporttype, periodtypeid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();
            year = Request.QueryString["year"].ToString().Trim();
            month = Request.QueryString["month"].ToString().Trim();
            reporttype = Request.QueryString["reporttype"].ToString().Trim();
            assetCode = Request.QueryString["assettypeid"].ToString().Trim();
            periodtypeid = Request.QueryString["periodtypeid"].ToString().Trim();


            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();


            if (reporttype == "Summary")
            {

                dt = qry.QueryReportPaymentSummary(branch, reporttype, assetCode, periodtypeid, month, year, date1, date2, "spMPMFCustRptPaymentSummary");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptReportPaymentSummary.rdlc");
            }

            else
            {

                dt = qry.QueryReportPaymentDetail(branch, reporttype, assetCode, periodtypeid, date1, date2, year, month, "spMPMFCustRptPaymentDetail");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptReportPaymentDetail.rdlc");
            }


            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", branch);
            ReportParameter p2 = new ReportParameter("date1", date1);
            ReportParameter p3 = new ReportParameter("date2", date2);
            ReportParameter p4 = new ReportParameter("reporttype", reporttype);
            ReportParameter p5 = new ReportParameter("periodtypeid", periodtypeid);
            ReportParameter p6 = new ReportParameter("assetCode", assetCode);
            ReportParameter p7 = new ReportParameter("year", year);
            ReportParameter p8 = new ReportParameter("month", month);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }

    }
}