﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_DailyInsurance
{
    public partial class MPMFCust_DailyInsurance : WebFormBase
    //public partial class MPMFCust_DailyInsurance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadbranch();
            }
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";

            if (dtStart.Text == "")
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker = dtStart.Text;

            if (dtEnd.Text == "")
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker1 = dtEnd.Text;

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("MPMFCust_DailyInsuranceView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&date1=" + datepicker.ToString() + "&date2=" + datepicker1.ToString() + "&status=" + ddlStatus.SelectedValue.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MPMFCust_DailyInsurance.aspx");
        }
    }
}