﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_DailyInsurance
{
    public partial class MPMFCust_DailyInsuranceView : System.Web.UI.Page
    {
        private string branch, branchname, date1, date2, status;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();
            status = Request.QueryString["status"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            if (status == "0")
            {
                dt = qry.QueryReportspAnjCustRptDailyInsurance(branch, date1, date2, "uspAnjCustRptDailyInsurance");
            }
            else
            {
                dt = qry.QueryReportspAnjCustRptDailyInsurance(branch, date1, date2, status, "uspAnjCustRptDailyInsurance");
            }
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDailyInsurance.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", branch);
            ReportParameter p2 = new ReportParameter("tglfrom", date1);
            ReportParameter p3 = new ReportParameter("tglto", date2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            //GenerateToExcel(ReportViewer1);
        }

        private void GenerateToExcel(ReportViewer reportViewer1)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=RDLC." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}