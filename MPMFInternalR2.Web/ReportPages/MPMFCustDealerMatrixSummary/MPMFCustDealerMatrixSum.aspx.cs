﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.MPMFCustDealerMatrixSummary
{
    public partial class MPMFCustDealerMatrixSum : WebFormBase
    //public partial class MPMFCustDealerMatrixSum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Add("loginID", "MellysaK");
            //Session.Add("sesBranchId", "999");

            if (!IsPostBack)
            {
                loadArea();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                //loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }
        protected void ddlRptType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblwarddlRptType.Visible = false;
            ddltahun.SelectedIndex = 0;
            ddlbulan.SelectedIndex = 0;
            ddlProduct.SelectedIndex = 0;
            ddlArea.SelectedIndex = 0;
        }
        protected void ddltahun_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblwarddltahun.Visible = false;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            

            if (this.ddlRptType.SelectedValue.ToString() == "00")
            {
                this.lblwarddlRptType.Visible = true;
                return;
            }
            if (this.ddltahun.SelectedValue.ToString() == "00")
            {
                this.lblwarddltahun.Visible = true;
                return;
            }

            Response.Redirect("MPMFCustDealerMatrixSumView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                        + "&RptTypeId=" + ddlRptType.SelectedValue.ToString().Trim() + "&RptTypeName=" + ddlRptType.SelectedItem.Text.Trim()
                        + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim()
                        + "&ProdId=" + ddlProduct.SelectedValue.ToString().Trim() + "&ProdName=" + ddlProduct.SelectedItem.Text.Trim()
                        + "&year=" + ddltahun.SelectedValue.ToString().Trim() + "&month=" + ddlbulan.SelectedValue.ToString().Trim()
                        + "&monthname=" + ddlbulan.SelectedItem.Text.Trim()
                        );




        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MPMFCustDealerMatrixSum.aspx");
        }


    }
}