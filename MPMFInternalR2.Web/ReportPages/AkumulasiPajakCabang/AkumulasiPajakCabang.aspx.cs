﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AkumulasiPajakCabang
{
    public partial class AkumulasiPajakCabang : WebFormBase
    //public partial class AkumulasiPajakCabang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //base.CurrentUserContext.OfficeCode.ToString() = "999";
            //base.CurrentUserContext.UserId.ToString() = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
            loadsupplier();
            loadYear();
        }

        private void loadYear()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryGetYear();
            this.ddlyear.DataSource = set;
            this.ddlyear.DataTextField = "year";
            this.ddlyear.DataValueField = "year";
            this.ddlyear.DataBind();
            this.ddlyear.SelectedIndex = 0;
            set.Dispose();
        }

        private void loadsupplier()
        {
            if ((ddlbranch.SelectedValue != "Select One" & ddlbranch.SelectedValue != "All") & ddlbranch.SelectedValue != "All")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QuerySupplierByBranch(ddlbranch.SelectedValue.Trim(), ddlyear.SelectedValue.Trim());
                ddlsupplier.DataSource = ds;
                ddlsupplier.DataTextField = "suppliername";
                ddlsupplier.DataValueField = "supplierid";
                ddlsupplier.DataBind();
                //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                {
                    ddlsupplier.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                ddlsupplier.SelectedIndex = 0;
            }
        }

        private void loadbranch()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtmarketing.Text.Trim() == "")
                {
                    lblwarmarketing.Visible = true;
                }
                else
                {
                    lblwarmarketing.Visible = false;
                    DataSet set = new DataSet();
                    DataTable dt = new DataTable();
                    set = new QueryConnection().spMPMFCust_TaxGrid(this.ddlbranch.SelectedValue.ToString(), this.txtmarketing.Text.ToString().Trim(), this.ddlyear.SelectedValue.ToString().Trim(), this.ddlsupplier.SelectedValue.ToString().Trim(), "spMPMFCust_TaxGrid");
                    dt = set.Tables[0];
                    this.gvRefTableTax.DataSource = dt;
                    this.gvRefTableTax.DataBind();
                    set.Dispose();

                }
            }
            catch (Exception ex)
            {
                Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Kesalahan pada sistem!' " + ex.ToString() + "'); </script>");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("AkumulasiPajakCabang.aspx");
        }

        protected void gvRefTableTax_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Print")
            {
                //string marketing = Convert.ToString(e.CommandArgument.ToString());
                int rowIndex = ((e.CommandSource as LinkButton).NamingContainer as GridViewRow).RowIndex;
                var supplierKtpId = ((Label)gvRefTableTax.Rows[rowIndex].FindControl("lblsupplierktpid")).Text.Trim();
                var supplieremployeename = ((Label)gvRefTableTax.Rows[rowIndex].FindControl("lblName")).Text.Trim();
                var SupplierEmployeeid = ((HiddenField)gvRefTableTax.Rows[rowIndex].FindControl("lblSupplierEmployeeid")).Value.Trim();

                Response.Redirect("AkumulasiPajakCabangView.aspx?SupplierEmployeeid=" + SupplierEmployeeid.Trim() + "&supplierktpid=" + supplierKtpId.Trim() + "&supplieremployeename=" + supplieremployeename.Trim() + "&branchid=" + ddlbranch.SelectedValue.Trim() + "&year=" + ddlyear.SelectedValue.Trim() + "");
            }
        }

        protected void gvRefTableTax_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (this.txtmarketing.Text.ToString().Trim() == "")
            {
                this.lblwarmarketing.Visible = true;
            }
            else
            {
                this.lblwarmarketing.Visible = false;
                this.gvRefTableTax.PageIndex = e.NewPageIndex;
                DataSet set = new DataSet();
                DataTable dt = new DataTable();
                set = new QueryConnection().spMPMFCust_TaxGrid(this.ddlbranch.SelectedValue.ToString(), this.txtmarketing.Text.ToString().Trim(), this.ddlyear.SelectedValue.ToString().Trim(), this.ddlsupplier.SelectedValue.ToString().Trim(), "spMPMFCust_TaxGrid");
                dt = set.Tables[0];
                this.gvRefTableTax.DataSource = dt;
                this.gvRefTableTax.DataBind();
                set.Dispose();
            }
        }

        protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadsupplier();
        }

        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((this.ddlbranch.SelectedItem.Value != "Select One") & (this.ddlbranch.SelectedItem.Value != "All") & (this.ddlbranch.SelectedItem.Value != "All"))
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QuerySupplierByBranch(ddlbranch.SelectedValue.ToString().Trim(), ddlyear.SelectedValue.ToString().Trim());
                ddlsupplier.DataSource = ds;
                ddlsupplier.DataTextField = "suppliername";
                ddlsupplier.DataValueField = "supplierid";
                ddlsupplier.DataBind();
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                {
                    ddlsupplier.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                ddlsupplier.SelectedIndex = 0;

                ds.Dispose();
            }
        }
    }
}