﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AkumulasiPajakCabang
{
    public partial class AkumulasiPajakCabangView : System.Web.UI.Page
    {
        private string strbranchid, strsupplierktpid, stryear;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            stryear = base.Request.QueryString["year"].ToString().Trim();
            strsupplierktpid = base.Request.QueryString["supplierktpid"].ToString().Trim();
            strbranchid = base.Request.QueryString["branchid"].ToString().Trim();

            DataTable dt = new DataTable();
            dt = new QueryConnection().QueryMPMFCust_RptTax(strsupplierktpid, strbranchid, stryear, "spMPMFCust_RptTaxUser");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/reporttaxuseranjf.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}