﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.CustomerOverdueByCollector
{
    public partial class RptCustomerByCollectorView : System.Web.UI.Page
    {
        private string branch, branchname, periode, startBucket, endingBucket, reporttype, areaid, areaname, endingBucketValue, startBucketValue, loginid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            periode = Request.QueryString["period"].ToString().Trim();
            startBucket = Request.QueryString["startBucket"].ToString().Trim();
            startBucketValue = Request.QueryString["startBucketValue"].ToString().Trim();
            endingBucket = Request.QueryString["endingBucket"].ToString().Trim();
            endingBucketValue = Request.QueryString["endingBucketValue"].ToString().Trim();
            reporttype = Request.QueryString["reporttype"].ToString().Trim();
            areaid = Request.QueryString["areaid"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            if (reporttype == "summary")
            {
                dt = qry.QueryReportCustomerByCollector(branch, periode, startBucket, endingBucket, reporttype, areaid, "spSAFCustomerOD");
            }
            else
            {
                dt = qry.QueryReportCustomerByCollector(branch, periode, startBucket, endingBucket, reporttype, areaid, "spSAFCustomerODDetail");
            }

            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            if (reporttype == "summary")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptCustomerByCollectorSummary.rdlc");
            }
            else
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptCustomerByCollectorDetail.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branch", branch);
            ReportParameter p2 = new ReportParameter("branchname", branchname);
            ReportParameter p3 = new ReportParameter("periode", periode);
            ReportParameter p4 = new ReportParameter("startBucket", startBucketValue);
            ReportParameter p5 = new ReportParameter("reporttype", reporttype);
            ReportParameter p6 = new ReportParameter("loginid", loginid);
            ReportParameter p7 = new ReportParameter("endingBucket", endingBucketValue);
            ReportParameter p8 = new ReportParameter("areaname", areaname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}