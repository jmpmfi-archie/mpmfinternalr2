﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_RptAssetManagement
{
    public partial class MPMFRptAssetManagementStock : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session.Add("loginID", "MellysaK");
                //Session.Add("sesBranchId", "'999'");
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strStartDate = "";
            //string strEndDate = "";


            if (dtStart.Text == "")
            {
                lblwardate.Visible = true; return;
            }
            //strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
            {
                lblwardate.Visible = false;
                strStartDate = dtStart.Text;
                Response.Redirect("MPMFRptAssetManagementStockView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&StartDate=" + strStartDate.ToString() + "");
            }

                //if (dtEnd.Text == "")
                //    strEndDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strEndDate = dtEnd.Text;

                //DateTime date1 = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //DateTime date2 = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                //if (DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                //{
                //    lblwardate.Visible = true; return;
                //}
                //else if ((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year) >= 6)
                //{
                //    lblwardate.Text = "* Period Date may not exceed 6 months";
                //    lblwardate.Visible = true; return;
                //}
                //else
                //{
                //    lblwardate.Visible = false;
                //}

                //if (this.ddlRptType.SelectedValue.ToString() == "")
                //{
                //    this.lblwarddlProduct.Visible = true;
                //    return;
                //}
                //else
                //{
                //    this.lblwarddlProduct.Visible = false;
                //    Response.Redirect("MPMFRptAssetManagementStockView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&StartDate=" + strStartDate.ToString() + "");
                //}
            }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MPMFRptAssetManagementStock.aspx");
        }
    }
}