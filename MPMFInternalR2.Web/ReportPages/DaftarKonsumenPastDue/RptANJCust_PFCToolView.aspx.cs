﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.DaftarKonsumenPastDue
{
    public partial class RptANJCust_PFCToolView : System.Web.UI.Page
    {
        private string branchid, ovd, collectorid, order, AssetTypeID, Description;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            ovd = Request.QueryString["ovd"].ToString().Trim();
            collectorid = Request.QueryString["collectorid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspAnjCust_PFCTool(branchid, ovd, collectorid, "spAnjCust_PFCTool");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptReportANJF_PFCTool.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}