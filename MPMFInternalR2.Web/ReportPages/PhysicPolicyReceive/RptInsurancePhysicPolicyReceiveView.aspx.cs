﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.PhysicPolicyReceive

{
    public partial class RptInsurancePhysicPolicyReceiveView : System.Web.UI.Page
    {
        private string branch, reportType, endDate, startDate, branchname;
         

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }
        protected void bindreport()
        {
            branch = Request.QueryString["branchId"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            reportType = Request.QueryString["reportType"].ToString().Trim();
            startDate = Request.QueryString["startDate"].ToString().Trim();
            endDate = Request.QueryString["endDate"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            
            if (reportType == "1")
            {
                dt = qry.QueryReportPhysicPolicyReceive(branch, startDate, endDate, reportType, "spMPMFPhysicPolicyReceive");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptPhysicPolicyReceive.rdlc");
            }
            else
            {
                dt = qry.QueryReportPhysicPolicyReceive(branch, startDate, endDate, reportType, "spMPMFPhysicPolicyReceive2");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptInvoiceRequestPayment.rdlc");
            }
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchId", branch);
            ReportParameter p2 = new ReportParameter("branchname", branchname);
            ReportParameter p3 = new ReportParameter("startDate", startDate);
            ReportParameter p4 = new ReportParameter("endDate", endDate);
            ReportParameter p5 = new ReportParameter("reportType", reportType);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}