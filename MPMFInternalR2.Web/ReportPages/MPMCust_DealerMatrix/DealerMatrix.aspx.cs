﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.MPMCust_DealerMatrix
{
    public partial class DealerMatrix : WebFormBase
    //public partial class DealerMatrix : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";
                loadData();
            }
        }

        private void loadData()
        {
            loadtipematrix();
            loadtahun();
            loadbulan();
        }

        private void loadbulan()
        {
            ddlbulan.Items.Insert(0, new ListItem("Select One", "000"));
            ddlbulan.Items.Insert(1, new ListItem("January", "1"));
            ddlbulan.Items.Insert(2, new ListItem("February", "2"));
            ddlbulan.Items.Insert(3, new ListItem("Maret", "3"));
            ddlbulan.Items.Insert(4, new ListItem("April", "4"));
            ddlbulan.Items.Insert(5, new ListItem("Mei", "5"));
            ddlbulan.Items.Insert(6, new ListItem("Juni", "6"));
            ddlbulan.Items.Insert(7, new ListItem("Juli", "7"));
            ddlbulan.Items.Insert(8, new ListItem("Agustus", "8"));
            ddlbulan.Items.Insert(9, new ListItem("September", "9"));
            ddlbulan.Items.Insert(10, new ListItem("Oktober", "10"));
            ddlbulan.Items.Insert(11, new ListItem("November", "11"));
            ddlbulan.Items.Insert(12, new ListItem("Desember", "12"));

            ddltipematrix.SelectedIndex = 0;
        }

        private void loadtahun()
        {
            ddltahun.Items.Insert(0, new ListItem("Select One", "000"));
            DateTime today = DateTime.Today;
            DateTime month = new DateTime(today.Year, today.Month, 1);


            DateTime Y = new DateTime(today.Year, today.Month, 1);

            for (int m = -2; m <= 1; m++)
            {
                DateTime previousmont = Y.AddYears(m); ;
                ListItem list = new ListItem();
                list.Text = previousmont.ToString("yyyy");
                list.Value = previousmont.ToString("yyyy");
                ddltahun.Items.Add(list);
            }
        }

        private void loadtipematrix()
        {
            ddltipematrix.Items.Insert(0, new ListItem("Select One", "000"));
            ddltipematrix.Items.Insert(1, new ListItem("New Bike", "001"));
            ddltipematrix.Items.Insert(2, new ListItem("New Car", "002"));
            ddltipematrix.Items.Insert(3, new ListItem("Used Car", "003"));

            ddltipematrix.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddltipematrix.SelectedValue.ToString() == "000")
            {
                lblwartipe.Visible = true;
                return;
            }
            else
            {
                lblwartipe.Visible = false;
            }

            if (ddltahun.SelectedValue.ToString() == "000")
            {
                lblwartahun.Visible = true;
                return;
            }
            else
            {
                lblwartahun.Visible = false;
            }

            if (ddlbulan.SelectedValue.ToString() == "000")
            {
                lblwarbulan.Visible = true;
                return;
            }
            else
            {
                lblwarbulan.Visible = false;
            }

            Response.Redirect("RptDealerMatixView.aspx?tahun=" + ddltahun.SelectedValue.ToString().Trim() + "&bulan=" + ddlbulan.SelectedValue.ToString().Trim() + "&bulanname=" + ddlbulan.SelectedItem.ToString() + "&tipemetrik=" + ddltipematrix.SelectedValue.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("DealerMatrix.aspx");
        }
    }
}