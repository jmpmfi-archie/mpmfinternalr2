﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMCust_DealerMatrix
{
    public partial class RptDealerMatixView : System.Web.UI.Page
    {
        private string tahun, bulan, bulanname, tipemetrik;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            tahun = Request.QueryString["tahun"].ToString().Trim();
            bulan = Request.QueryString["bulan"].ToString().Trim();
            bulanname = Request.QueryString["bulanname"].ToString().Trim();
            tipemetrik = Request.QueryString["tipemetrik"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (tipemetrik == "001")
            {
                dt = qry.QueryReportspMPMFCust_RptDealerMatrix(tahun, bulan, "spMPMFCust_RptDealerMatrixNewBike");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixNewBike.rdlc");
            }
            else if (tipemetrik == "002")
            {
                dt = qry.QueryReportspMPMFCust_RptDealerMatrix(tahun, bulan, "spMPMFCust_RptDealerMatrixNewCar");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixNewCar.rdlc");
            }
            else if (tipemetrik == "003")
            {
                dt = qry.QueryReportspMPMFCust_RptDealerMatrix(tahun, bulan, "spMPMFCust_RptDealerMatrixUsedCar");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixUseCar.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("bulan", bulan);
            ReportParameter p2 = new ReportParameter("tahun", tahun);
            ReportParameter p3 = new ReportParameter("bulanname", bulanname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}