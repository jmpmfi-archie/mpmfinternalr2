﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.DailyNPL
{
    public partial class rptDailyNPLView : System.Web.UI.Page
    {
        private string AgingDate, Branch, Product, ReportType;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            AgingDate = Request.QueryString["AgingDate"].ToString().Trim();
            Branch = Request.QueryString["Branch"].ToString().Trim();
            Product = Request.QueryString["Product"].ToString().Trim();
            ReportType = Request.QueryString["ReportType"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (ReportType == "1")
            {
                dt = qry.QueryReportspMPMFCustRptDailyNPL(Branch, AgingDate, Product, "spMPMFCustRptDailyNPL");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/DailyNPLSummary.rdlc");
            }
            else
            {
                dt = qry.QueryReportspMPMFCustRptDailyNPL(Branch, AgingDate, Product, "spMPMFCustRptDailyNPLDetail");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/DailyNPLDtl.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", Branch);
            ReportParameter p2 = new ReportParameter("agingdate", AgingDate);
            ReportParameter p3 = new ReportParameter("productjrn", Product);
            ReportParameter p4 = new ReportParameter("tglAging", AgingDate);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}